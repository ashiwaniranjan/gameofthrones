package framework.gameofthrones.Daenerys;

import framework.gameofthrones.Aegon.StaticData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;

/**
 * Created by ashish.bajpai on 17/04/15.
 */
public class Translators {
    private StaticData data = new StaticData();
    //private String FILE_NAME = "qaenvironment.xml";
    private String ENV_FILE_NAME = data.EnvironmentFilePath;
    //public String ENV_FILE_NAME = "/Users/ashish.bajpai/environment_stage1.xml";
/*

    private static UIElements jaxbXMLToObject_uielements() {
        try {
            JAXBContext context = JAXBContext.newInstance(UIElements.class);
            Unmarshaller un = context.createUnmarshaller();
            //environment emp = (environment) un.unmarshal(new File(FILE_NAME));
            return (UIElements) un.unmarshal(new File(UI_ELEMENTS_FILE_NAME));
            //return uielem;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static void jaxbObjectToXML_uielements(UIElements elements) {

        try {
            JAXBContext context = JAXBContext.newInstance(UIElements.class);
            Marshaller m = context.createMarshaller();
            //for pretty-print XML in JAXB
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Write to System.out for debugging
            // m.marshal(emp, System.out);

            // Write to File
            m.marshal(elements, new File(UI_ELEMENTS_FILE_NAME));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
*/


    public  environment jaxbXMLToObject(InputStream is) {
        try {
            JAXBContext context = JAXBContext.newInstance(environment.class);
            Unmarshaller un = context.createUnmarshaller();
            environment emp = (environment) un.unmarshal(is);
            return emp;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }


    private  void jaxbObjectToXML(environment emp) {

        try {
            JAXBContext context = JAXBContext.newInstance(environment.class);
            Marshaller m = context.createMarshaller();
            //for pretty-print XML in JAXB
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Write to System.out for debugging
            // m.marshal(emp, System.out);

            // Write to File
            m.marshal(emp, new File(ENV_FILE_NAME));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
