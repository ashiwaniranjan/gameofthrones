package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ashish.bajpai on 05/02/15.
 */
@XmlRootElement(name = "Framework")
public class Framework {

    //private List<Connstring> connectiondetails;
    private paths paths;
    private String mailformatfolder;

    public paths getPaths() {
        return paths;
    }

    @XmlElement(name = "paths")
    public void setPaths(paths paths) {
        this.paths = paths;
    }

    @XmlElement(name = "mailformatfolder")
    public void setMailformatfolder(String mailformatfolder) {
        this.mailformatfolder = mailformatfolder;
    }

    public String getMailformatfolder() {
        return mailformatfolder;
    }




}
