package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="excelconfig")
public class excelconfig {
	
 private List<Excel>excel;

public List<Excel> getExcel() {
	return excel;
}

@XmlElement
public void setExcel(List<Excel> excel) {
	this.excel = excel;
}	
}
