package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by abhijit.pati on 20/09/16.
 */

@XmlRootElement(name = "kafka")
public class KafkaConnection {

    private Connectionstring connectionstrings;

    public Connectionstring getConnectionstrings() {
        return connectionstrings;
    }

    @XmlElement(name = "connectionstrings")
    public void setConnectionstrings(Connectionstring connectionstrings) {
        this.connectionstrings = connectionstrings;
    }
}

