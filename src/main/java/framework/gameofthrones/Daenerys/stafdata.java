package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.List;

@XmlRootElement(name = "stafdata")
public class stafdata {
	
	private excelconfig excelconfig;
	private keyvaluepairs keyvaluepairs;

	public keyvaluepairs getKeyvaluepairs() {
		return keyvaluepairs;
	}

	@XmlElement
	public void setKeyvaluepairs(keyvaluepairs keyvaluepairs) {
		this.keyvaluepairs = keyvaluepairs;
	}

	public excelconfig getExcelconfig() {
		return excelconfig;
	}



	@XmlElement
	public void setExcelconfig(excelconfig excelconfig) {
		this.excelconfig = excelconfig;
	}



	public HashMap<String, String> getKeyValuePairs() {
		HashMap<String, String> hm = new HashMap<String, String>();
		List<keyvaluepair> kvp = keyvaluepairs.getKeyvaluepair();
		for (keyvaluepair kkp : kvp) {
			hm.put(kkp.getKey(), kkp.getValue());
		}
		return hm;
	}
	public HashMap<String, String> getExcelConfig() {
		HashMap<String, String> hm = new HashMap<String, String>();
		List<Excel> kvp = excelconfig.getExcel();
		for (Excel kkp : kvp) {
			hm.put(kkp.getKey(), kkp.getValue());
		}
		return hm;
	}
	
}
