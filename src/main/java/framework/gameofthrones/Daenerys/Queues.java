package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Queues")
public class Queues {

	private List<QueueConnectiondetails> queueConnectiondetails;


	public List<QueueConnectiondetails> getQueueConnectiondetails() {
		return queueConnectiondetails;
	}

	@XmlElement(name = "connectiondetails")
	public void setQueueConnectiondetails(List<QueueConnectiondetails> queueConnectiondetails) {
		this.queueConnectiondetails = queueConnectiondetails;
	}


	public QueueConnectiondetails GetQueueConnectionDetails(String hostName){
		QueueConnectiondetails qcd=new QueueConnectiondetails();
		for (QueueConnectiondetails details: queueConnectiondetails) {
			if (details.getHostname().trim().toLowerCase().equals(hostName)){
				qcd=details;
			}
		}
		return qcd;
	}



}
