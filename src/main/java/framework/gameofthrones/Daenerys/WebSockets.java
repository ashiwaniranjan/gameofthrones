package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "websocket")
public class WebSockets {
    private List<WebSocket> webSockets;

    public List<WebSocket> getWebSockets() {
        return webSockets;
    }

    @XmlElement()
    public void setWebSockets(List<WebSocket> webSockets) {
        this.webSockets = webSockets;
    }
}
