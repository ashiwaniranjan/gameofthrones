package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "excel")
public class Excel {
	// String inputexceldatafilepath=null;
	// String sheetname=null;
	//
	// public String getInputexceldatafilepath() {
	// return inputexceldatafilepath;
	// }
	// @XmlAttribute
	// public void setInputexceldatafilepath(String inputexceldatafilepath) {
	// System.out.println(inputexceldatafilepath);
	// this.inputexceldatafilepath = inputexceldatafilepath;
	// }
	// public String getSheetname() {
	// return sheetname;
	// }
	// @XmlAttribute
	// public void setSheetname(String sheetname) {
	// System.out.println(sheetname);
	// this.sheetname = sheetname;
	// }

	String key;
	String value;

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute
	public void setValue(String value) {
		this.value = value;
	}

	@XmlAttribute
	public void setKey(String key) {
		this.key = key;
	}

}
