package framework.gameofthrones.Daenerys;/*
package Daenerys;

*/
/**
	 * Author : ashish bajpai
     * @Since
*//*


import org.apache.log4j.Logger;

import java.io.*;
import java.util.Properties;


public class PropertyReader
{
	Names entities = new Names();
    String propertiesfilename = entities.getPropertiesFileName();
	Properties props = new Properties();
	static Logger log = Logger.getLogger(PropertyReader.class);
	FileInputStream fis;
	InputStream is;
	private boolean isexternalfolder = true;
	private String FilepathFull = "";
	

	*/
/**
	 * Author : ashish.bajpai
	 * PropertyReader constructor which allows users to load  properties file which comes default with the properties
	 *//*

	public PropertyReader() 
	{
		try 
		{
			FilepathFull = propertiesfilename;
			isexternalfolder = false;
            //printallfilesincurrentdirectory();
            System.out.println("System.getProperty(\"user.dir\") = " + System.getProperty("user.dir"));
            is = this.getClass().getResourceAsStream(propertiesfilename);
			props.load(is);
			
		} 
		catch (IOException e) 
		{
			log.error("exception occurred while reading properties file from path "+ propertiesfilename);
			e.printStackTrace();
		}
	}


	*/
/**
	 * @author : ashish.bajpai
	 * 
	 * 
	 * PropertyReader constructor which allows users to load  properties file
	 * This takes single parameter which is path to a single property file which contains your desired properties.
	 *//*

	public PropertyReader(String PathofBaseConfigurationFolder) 
	{
		try 
		{
			isexternalfolder = true;
			//FilepathFull = PathofBaseConfigurationFolder + "/" + propertiesfilename;
            FilepathFull = PathofBaseConfigurationFolder + "/" + propertiesfilename;
			log.info("properties file used for framework initialize is :"+ FilepathFull);
			fis = new FileInputStream(FilepathFull);
			props.load(fis);
		} catch (IOException e) 
		{
			log.error("exception occurred while reading properties file from path "+ PathofBaseConfigurationFolder + "/" + propertiesfilename);
			e.printStackTrace();
		}
	}
	
	*/
/**
	 * Author : ashish.bajpai
	 * 
	 * PropertyReader constructor which allows users to load multiple properties files
	 * This accepts paths to individual files as an array and returns a loaded file which can be used to access properties at run time.
	 *//*

	public PropertyReader(String[] Pathofpropertiesfile) 
	{
		try 
		{
			for(String pathoffile : Pathofpropertiesfile)
			{
				fis = new FileInputStream(pathoffile);
				props.load(fis);
			}
			
		} catch (IOException e) 
		{
			log.error("exception occurred while reading properties file from path ");
			e.printStackTrace();
		}
	}
	
	private void printvalues()
	{
		for(Object keys:props.keySet())
		{
			System.out.println("Key = '"+keys.toString()+"' has value = '");
		}
	}

	private void printallfilesincurrentdirectory()
	{
		String path = ".";

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++)
		{
			files = listOfFiles[i].getName();
			System.out.println(files);
		}
	}
	
	
	private String readproperty(String propertyname)
	{
		return props.getProperty(propertyname);
	}
	
	public void SetProperty(String Key, String Value)
	{
		try
		{
			FileInputStream in = new FileInputStream(FilepathFull);
	        Properties props = new Properties();
	        props.load(in);
	        in.close();
	
	        FileOutputStream out = new FileOutputStream(FilepathFull);
	        props.setProperty(Key, Value);
	        props.store(out, null);
	        out.close();

		} catch (Exception e)
		{
			log.info("Exception Occurred while setting property "+Key);
		}
	}

	*/
/**
	 * Author : ashish.bajpai
	 * 
	 * Allows user to read value of a key from loaded properties files. Accepts the parameter as the  key name in string format 
	 *//*

	
	public String getPropertyValue(String propertyname)
	{
		return readproperty(propertyname);
	}

    public void ReadAllProperties(String Propertyname)
    {

    }

}
*/
