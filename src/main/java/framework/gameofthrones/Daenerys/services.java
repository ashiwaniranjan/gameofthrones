package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by ashish.bajpai on 05/02/15.
 */
@XmlRootElement(name = "services")
public class services
{
    private endpoints endpoints;
    private apis apis;
    public endpoints getEndpoints() {
        return endpoints;
    }

    @XmlElement
    public void setEndpoints(endpoints endpnt) {
        this.endpoints = endpnt;
    }

    public apis getApis() {
        return apis;
    }

    @XmlElement
    public void setApis(apis apis) {
        this.apis = apis;
    }

    public endpoint GetServiceDetails(String ServiceName){
        endpoint ep = new endpoint();
        List<endpoint> collection = endpoints.getEndpoint();

        for (endpoint e: collection) {

            if (e.getServicename().trim().toLowerCase().equals(ServiceName)){
                if(e.getBaseurl().toString().contains("${env}")) {
                    String env = (System.getProperty("sit-env") != null) ?  System.getProperty("sit-env") : "dev1";
                    e.setBaseurl(e.getBaseurl().replace("${env}", env));
                }
                ep = e;
            }

        }
        return ep;
    }
}
