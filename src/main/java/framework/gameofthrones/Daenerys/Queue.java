package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "queue")
public class Queue{
	
	private String name;
	private String exchangekey;
	private String routingkey;
	private String vhost;
	private boolean hasconsumers = true;
	
	public String getName() {
		return name;
	}
	
	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}
	
	public String getExchangekey() {
		return exchangekey;
	}
	
	@XmlAttribute
	public void setExchangekey(String exchangekey) {
		this.exchangekey = exchangekey;
	}
	
	public String getRoutingkey() {
		return routingkey;
	}
	
	@XmlAttribute
	public void setRoutingkey(String routingkey) {
		this.routingkey = routingkey;
	}

	public String getVhost() {
		return vhost;
	}

	@XmlAttribute
	public void setVhost(String vhost) {
		if(vhost == null){
			this.vhost = "/";
		}else{
			this.vhost = vhost;
		}
	}

	public boolean getHasconsumers() {
		return hasconsumers;
	}

	@XmlAttribute
	public void setHasconsumers(boolean hasconsumers) {
		this.hasconsumers = hasconsumers;
	}


	
}
