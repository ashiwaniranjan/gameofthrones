package framework.gameofthrones.Daenerys;

import framework.gameofthrones.Aegon.PropertiesHandler;
import framework.gameofthrones.Aegon.StaticData;
import framework.gameofthrones.JonSnow.APIData;
import framework.gameofthrones.JonSnow.QueueData;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.*;

/**
 * Created by ashish.bajpai on 17/04/15.
 */
public class Setup implements ResourceLoaderAware
{
    Tools tools = new Tools();
    String envData = null;
    Resource envResource;
    private ResourceLoader resourceLoader;
    private String environment;
    private String isDockinsEnabled;
    private String dockEnvName;
    private static Logger log = LoggerFactory.getLogger(Setup.class);
    private String EnvironmentFilePath = null;

    public Setup(){
        //StaticData data = new StaticData();
        init();
    }

    public Setup(PropertiesHandler properties){
        //StaticData data = new StaticData(properties);
        init(properties);
    }

    public Setup(String environmentname){
        //StaticData data = new StaticData(properties);
        init(environmentname);
    }

    public void reloadenvironment(PropertiesHandler props){
        init(props);
    }


    public void init()
    {
        try{
            /*System.out.println("getEnvironmentUrl() = " + getEnvironmentUrl());
            envResource = resourceLoader.getResource(getEnvironmentUrl());
            envData = IOUtils.toString(envResource.getInputStream());*/
            //InputStream is = new FileInputStream("/Users/ashish.bajpai/environment_stage1.xml");
            InputStream is = new FileInputStream("./Data/Environments/environment_stage2.xml");
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
                String fileAsString = sb.toString();
            envData =  fileAsString;


        }catch(Exception ex){
            log.debug("Error reading environment reading from URL, will try to search local");
            throw new RuntimeException("Error reading environment config", ex);
        }

    }

    public void init(PropertiesHandler properties)
    {
        try{
            StaticData state = new StaticData(properties);
            //state.getGetTestEnvironment();
            //InputStream is = new FileInputStream("./Data/Environments/environment_stage1.xml");
            System.out.println("state.getEnvironmentFilePath() = " + state.getEnvironmentFilePath());
            EnvironmentFilePath = state.getEnvironmentFilePath();
            InputStream is = new FileInputStream(state.getEnvironmentFilePath());
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            String fileAsString = sb.toString();
            envData =  fileAsString;


        }catch(Exception ex){
            log.debug("Error reading environment reading from URL, will try to search local");
            throw new RuntimeException("Error reading environment config", ex);
        }

    }

    public void init(String environmentname)
    {
        try{
            StaticData state = new StaticData(environmentname);
            //state.setGetTestEnvironment(environmentname);

            //state.getGetTestEnvironment();
            //InputStream is = new FileInputStream("./Data/Environments/environment_stage1.xml");
            System.out.println("state.getEnvironmentFilePath() new init = " + state.getEnvironmentFilePath());
            EnvironmentFilePath = state.getEnvironmentFilePath();
            InputStream is = new FileInputStream(state.getEnvironmentFilePath());
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            String fileAsString = sb.toString();
            envData =  fileAsString;


        }catch(Exception ex){
            log.debug("Error reading environment reading from URL, will try to search local");
            throw new RuntimeException("Error reading environment config", ex);
        }

    }

    public String getEnvironmentUrl()
    {
        String envXml = getEnvironmentXmlEndpoint();
        log.debug("Environment URL is: " + envXml);
        return envXml;
    }

    public environment getEnvironmentData()
    {
        Translators transform = new Translators();
     
       
        environment empFromFile = transform.jaxbXMLToObject(IOUtils.toInputStream(envData));
        //empFromFile = tools.modifyEnvFileForDockerInstances(isDockinsEnabled, dockEnvName, empFromFile);

        return empFromFile;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public String getEnvironment() {
        return environment;
    }


    private String getEnvironmentXmlEndpoint()
    {
        try{
            String currentDir = new File( "." ).getCanonicalPath();
            //System.out.println("\"file:///\"+data.getEnvironmentFilePath() = " + data.getEnvironmentFilePath());
            //return "file:///" + currentDir + File.separatorChar + "Data" + File.separatorChar + "configuration" + File.separatorChar + Environment.getEnvironment(environment).toString() + File.separatorChar + "environment_stage1.xml";
            //return "file:///"+data.getEnvironmentFilePath();
            //return "file://~/environment_stage1.xml";
            return "file://"+EnvironmentFilePath;
        }catch(Exception ex){
            throw new RuntimeException("Error reading environment xml", ex);
        }

    }

    public void setEnvironment(String environment) {this.environment = environment;}

    public void setIsDockinsEnabled(String isDockinsEnabled) {
        this.isDockinsEnabled = isDockinsEnabled;
    }

    public void setDockEnvName(String dockEnvName) {
        this.dockEnvName = dockEnvName;
    }

    public APIData GetAPIDetails(String ServiceName, String apiname){
        APIData data = new APIData(ServiceName,apiname, this.getEnvironmentData());
        return data;
    }

    public QueueData GetQueueDetails(String queueHost, String queueName){
        QueueData data = new QueueData(queueHost,queueName, this.getEnvironmentData());
        return data;
    }
}

