package framework.gameofthrones.Targaryen.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */

@Entity
@Table(name="environment")
public class EnvEntity extends BaseEntity{

	private static final long serialVersionUID = 4005154288922461026L;

	@Column(name = "`value`", nullable = true, length=50)
    private String environment;
	
	@Column(name = "`key`", nullable = true, length=50)
    private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
}
