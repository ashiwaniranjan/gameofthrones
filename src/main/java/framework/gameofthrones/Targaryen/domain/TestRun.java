package framework.gameofthrones.Targaryen.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;


/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "run")
public class TestRun extends BaseEntry{
	
		private static final long serialVersionUID = 2531284203436319869L;
		
		private String suitename;
		private Long testsuiteidfk;
		private String status;
		private String failure;
		private String environment;
		private int totalTests;
		private int totalFailed;
		private int totalSkipped;
		private int totalPassed;
		private int totalManual;
		private Date endedOn;
		private String branch;
		
		public Long getTestsuiteidfk() {
			return testsuiteidfk;
		}
		
		public void setTestsuiteidfk(Long testsuiteidfk) {
			this.testsuiteidfk = testsuiteidfk;
		}
		
		public String getStatus() {
			return status;
		}
		
		public void setStatus(String status) {
			this.status = status;
		}

		public String getFailure() {
			return failure;
		}

		public void setFailure(String failure) {
			this.failure = failure;
		}

		public String getEnvironment() {
			return environment;
		}

		public void setEnvironment(String environment) {
			this.environment = environment;
		}

		public int getTotalTests() {
			return totalTests;
		}

		public void setTotalTests(int totalTests) {
			this.totalTests = totalTests;
		}

		public int getTotalFailed() {
			return totalFailed;
		}

		public void setTotalFailed(int totalFailed) {
			this.totalFailed = totalFailed;
		}

		public int getTotalPassed() {
			return totalPassed;
		}

		public void setTotalPassed(int totalPassed) {
			this.totalPassed = totalPassed;
		}

		public int getTotalSkipped() {
			return totalSkipped;
		}

		public void setTotalSkipped(int totalSkipped) {
			this.totalSkipped = totalSkipped;
		}

		public Date getEndedOn() {
			return endedOn;
		}

		public void setEndedOn(Date endedOn) {
			this.endedOn = endedOn;
		}

		public String getBranch() {
			return branch;
		}

		public void setBranch(String branch) {
			this.branch = branch;
		}

		public int getTotalManual() {
			return totalManual;
		}

		public void setTotalManual(int totalManual) {
			this.totalManual = totalManual;
		}

		public String getSuitename() {
			return suitename;
		}

		public void setSuitename(String suitename) {
			this.suitename = suitename;
		}
		
		
		
}
