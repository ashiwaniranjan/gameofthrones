package framework.gameofthrones.Targaryen.domain;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "teams")
public class Team  extends BaseEntry{

	private static final long serialVersionUID = -8767438150054825349L;
	private int id;
	private String name;
	private List<Testsuite> testsuites = new ArrayList<Testsuite>();
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Testsuite> getTestsuites() {
		return testsuites;
	}
	
	public void setTestsuites(List<Testsuite> testsuites) {
		this.testsuites = testsuites;
	}
	
	public void addTestsuites(Testsuite testsuite) {
		this.testsuites.add(testsuite);
	}
	
}
