package framework.gameofthrones.Targaryen.domain;


import framework.gameofthrones.Targaryen.entity.TestConfigInfoEntity.TYPE;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "testconfiginfo")
public class TestConfigInfo extends BaseEntry{

	private static final long serialVersionUID = 2064096495829664804L;

	private String value;
	private TYPE type;
	private boolean enabled;
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public TYPE getType() {
		return type;
	}
	
	public void setType(TYPE type) {
		this.type = type;
	}

	public boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
		
}
