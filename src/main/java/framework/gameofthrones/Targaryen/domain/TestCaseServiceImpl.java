package framework.gameofthrones.Targaryen.domain;

import framework.gameofthrones.Targaryen.entity.*;
import framework.gameofthrones.Targaryen.repository.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class TestCaseServiceImpl implements TestCaseService {

    public static final int SUCCESS = 1;
    public static final int FAILURE = 2;
    public static final int SKIP = 3;
    private static Logger log = LoggerFactory.getLogger(TestCaseServiceImpl.class);

    @Autowired
    private TestcaseRepository testCaseRepository;

    @Autowired
    private TestsuiteRepository testSuiteRepository;

    @Autowired
    private TestresultRepository testResultRepository;

    @Autowired
    private TestrunRepository testRunRepository;

    @Autowired
    private TestconfigRepository testConfigRepository;

    @Autowired
    private TestconfiginfoRepository testConfigInfoRepository;

    @Autowired
    private EnvRepository envRepository;

    private boolean inited = false;

    private String environment;

    @Override
    public Testcase addTestCase(Testcase testcase) {

        Testsuite testsuite = new Testsuite();
        testsuite.setSuitename(testcase.getTestsuitename());
        TestcaseEntity testcaseEntity = null;
        Long suiteid = null;

        if(testcase.getTestsuite_id() == null) {
            testsuite = addTestSuite(testsuite, false);
            testcaseEntity = testCaseRepository.findByTestcasenameAndTestsuiteid(testcase.getTestcaseMethodName(), testsuite.getId());
            suiteid = testsuite.getId();
        }else{
            testcaseEntity = testCaseRepository.findByTestcasenameAndTestsuiteid(testcase.getTestcaseMethodName(), testcase.getTestsuite_id());
            suiteid = testcase.getTestsuite_id();
        }

        if(testcaseEntity == null){
            testcaseEntity = new TestcaseEntity();
        }

        testcaseEntity.setTestsuiteid(suiteid);
        testcaseEntity.setDescription(testcase.getDescription());
        testcaseEntity.setEnabled(testcase.isEnabled());
        testcaseEntity.setTestcase_id(testcase.getTestcaseMethodName());
        testcaseEntity.setGroups(testcase.getGroups());
        testcaseEntity.setIsManual(testcase.ismanual());
        testcaseEntity = testCaseRepository.save(testcaseEntity);
        return getTestcase(testcaseEntity);

    }

    @Override
    public Testcase updateTestCase(Testcase testcase) {

        Optional<TestcaseEntity> testcaseC = testCaseRepository.findById(testcase.getTestsuite_id());
        TestcaseEntity testcaseEntity = testcaseC.get();

        if(testcaseEntity != null){
            testcaseEntity.setDescription(testcase.getDescription());
            testcaseEntity.setEnabled(testcase.isEnabled());
            testcaseEntity.setGroups(testcase.getGroups());
            testcaseEntity.setTestcase_id(testcase.getTestcaseMethodName());
            testcaseEntity.setIsManual(testcase.ismanual());
            testcaseEntity = testCaseRepository.save(testcaseEntity);
            return getTestcase(testcaseEntity);
        }

        return null;
    }


    @Override
    public TestResult addTestResult(TestResult result) {
        TestresultEntity testResultE = new TestresultEntity();
        testResultE.setError(StringUtils.substring(result.getError(), 0, 9999));
        testResultE.setErrorMessage(StringUtils.substring(result.getErrormessage(),0,999));
        testResultE.setResult(result.getResult());
        testResultE.setTestrunid(result.getTestrunid());
        testResultE.setTestcaseid(result.getTestcaseid());
        testResultE.setStarttime(result.getStarttime());
        testResultE.setEndtime(result.getEndtime());
        testResultE.setTestparams(StringUtils.substring(result.getTestparams(), 0, 9999));
        testResultE = testResultRepository.saveAndFlush(testResultE);
        return getTestResult(testResultE);
    }

    @Override
    public TestRun addUpdateTestRun(TestRun testrun, boolean updateProgress) {
        TestrunEntity testRunEntity = new TestrunEntity();
        testRunEntity.setId(testrun.getId());
        testRunEntity.setTestsuiteid(testrun.getTestsuiteidfk());
        testRunEntity.setFailure(testrun.getFailure());
        testRunEntity.setCreatedBy(testrun.getCreatedBy());
        testRunEntity.setCreatedOn(testrun.getCreatedOn());
        testRunEntity.setLastModifiedOn(testrun.getLastModifiedOn());
        testRunEntity.setVersion(testrun.getVersion());
        testRunEntity.setTeststatus(TEST_STATUS.getStatus(testrun.getStatus()));
        testRunEntity.setEnvironment(testrun.getEnvironment());
        testRunEntity.setTotaltests(testrun.getTotalTests());
        testRunEntity.setPassed(testrun.getTotalPassed());
        testRunEntity.setFailed(testrun.getTotalFailed());
        testRunEntity.setSkipped(testrun.getTotalSkipped());
        testRunEntity.setManual(testrun.getTotalManual());
        testRunEntity.setBranch(testrun.getBranch());
        testRunEntity = this.testRunRepository.saveAndFlush(testRunEntity);

        if(updateProgress){
            TestsuiteEntity suiteE = this.testSuiteRepository.findById(testrun.getTestsuiteidfk()).get();
            log.info("Inside updateprogress");
            if(testrun.getTotalTests() > 0){
                log.info("Inside if updateprogress");
                float percentFailureSkipped = ((float) testrun.getTotalFailed() + (float) testrun.getTotalSkipped())/(float) testrun.getTotalTests() * 100;
                log.info("percentFailureSkipped " + percentFailureSkipped);
/*                if(percentFailureSkipped > 2 ){
                    suiteE.setProgress(-1);
                }else{
                    suiteE.setProgress(1);
                }*/
                suiteE.setProgress(1);
                log.info("Updated progress indicator");
            }else{
                suiteE.setProgress(0);
            }
            this.testSuiteRepository.saveAndFlush(suiteE);

        }
        return getTestrun(testRunEntity);
    }

    @Override
    public Testsuite getTestsuite(Long suiteid) {
        TestsuiteEntity suiteEntity = testSuiteRepository.findById(suiteid).get();
        return getTestSuite(suiteEntity);
    }

    @Override
    public List<Testcase> getTestcaseByTestName(String testname) {
        List<Testcase> testcases = new ArrayList<Testcase>();
        List<TestcaseEntity> testcaseEntitys = testCaseRepository.findByTestcasenameIgnoreCase(testname);
        for(TestcaseEntity testcaseE:testcaseEntitys ){
            testcases.add(getTestcase(testcaseE));
        }
        return testcases;
    }

    @Override
    public Testcase getTestcaseByTestId(Long id) {
        return getTestcase(testCaseRepository.findById(id).get());
    }

    @Override
    public List<Testcase> getAllTestcasesBySuiteName(String testsuite) {
        TestsuiteEntity testSuiteEntity = testSuiteRepository.findByTestsuitenameIgnoreCase(testsuite);
        return getTestcasesBySuiteId(testSuiteEntity.getId());
    }

    @Override
    public List<Testcase> getTestcasesBySuiteId(Long testsuiteId) {

        List<Testcase> testcases = new ArrayList<Testcase>();
        TestsuiteEntity testSuiteEntity = testSuiteRepository.findById(testsuiteId).get();
        if(testSuiteEntity != null){
            Long testsuiteidfk = testSuiteEntity.getId();
            List<TestcaseEntity> testcaseEs =  testCaseRepository.findByTestsuiteid(testsuiteidfk);
            for(TestcaseEntity testcaseE: testcaseEs){
                testcases.add(getTestcase(testcaseE));
            }
        }
        return testcases;
    }

    @Override
    public List<TestResult> getTestResults(Long testcaseid) {
        List<TestResult> testResults = new ArrayList<TestResult>();
        Testcase testcase = getTestcaseByTestId(testcaseid);
        List<TestresultEntity> testcaseEs = testResultRepository.findByTestcaseidfk(testcase.getId());
        if(testcase != null){
            for(TestresultEntity testresultE: testcaseEs){
                testResults.add(getTestResult(testresultE));
            }
        }
        return testResults;
    }


    @Override
    public List<TestRun> getTestRunsForSuiteId(Long suiteId) {
        List<TestRun> testruns = new ArrayList<TestRun>();
        Testsuite suite = getTestsuite(suiteId);
        if(suite != null){
            List<TestrunEntity> testResultEs = testRunRepository.findByTestsuiteid(suite.getId());
            if(testResultEs != null && !testResultEs.isEmpty()){
                for(TestrunEntity testResultE: testResultEs){
                    testruns.add(getTestrun(testResultE));
                }
            }
        }
        return testruns;
    }

    @Override
    public List<TestRun> getTestRunsForSuiteId(Long suiteId, Integer count, Integer page) {
        List<TestRun> testruns = new ArrayList<TestRun>();
        Testsuite suite = getTestsuite(suiteId);
        if(suite != null){
            page = (page == null || page < new Integer(0) ) ? new Integer(0): page;
            PageRequest request =
                    new PageRequest(page, count, Sort.Direction.DESC, "id");
            List<TestrunEntity> testResultEs = testRunRepository.findByTestsuiteid(suite.getId(), request);
            if(testResultEs != null && !testResultEs.isEmpty()){
                for(TestrunEntity testResultE: testResultEs){
                    testruns.add(getTestrun(testResultE));
                }
            }
        }
        return testruns;
    }

    @Override
    public Testsuite addTestSuite(Testsuite testsuite, boolean update) {
        TestsuiteEntity suiteEntity = testSuiteRepository.findByTestsuitenameIgnoreCase(testsuite.getSuitename());
        if(suiteEntity == null || update ){
            if(suiteEntity == null){
                suiteEntity = new TestsuiteEntity();
            }else if(!suiteEntity.getEnabled()){
                log.debug("Suite {} not enabled not logging results", testsuite.getSuitename());
                return null;
            }
            suiteEntity.setTestsuitename(testsuite.getSuitename());
            suiteEntity.setEnabled(testsuite.getEnabled());
            suiteEntity.setCore(testsuite.getCore());
            suiteEntity.setTeam(testsuite.getTeam());
            suiteEntity.setType(testsuite.getType());
            suiteEntity =  testSuiteRepository.saveAndFlush(suiteEntity);
        }

        return getTestSuite(suiteEntity);

    }


    @Override
    public Testcase getTestcaseBySuiteIdAndTestName(Long testSuiteId, String testMethodName) {
        TestcaseEntity testcaseEntity =  testCaseRepository.findByTestcasenameAndTestsuiteid(testMethodName, testSuiteId);
        return getTestcase(testcaseEntity);
    }

    private Testsuite getTestSuite(TestsuiteEntity suiteEntity){
        Testsuite testsuite = null;
        if(suiteEntity != null){
            testsuite = new Testsuite();
            testsuite.setSuitename(suiteEntity.getTestsuitename());
            testsuite.setId(suiteEntity.getId());
            testsuite.setVersion(suiteEntity.getVersion());
            testsuite.setCreatedOn(suiteEntity.getCreatedOn());
            testsuite.setLastModifiedOn(suiteEntity.getLastModifiedOn());
            testsuite.setCreatedBy(suiteEntity.getCreatedBy());
            testsuite.setEnabled(suiteEntity.getEnabled());
            testsuite.setProgress(suiteEntity.getProgress());
            if(suiteEntity.getTeam() == null){
                testsuite.setTeam("Other");
            }else{
                testsuite.setTeam(suiteEntity.getTeam());
            }
            testsuite.setType(suiteEntity.getType());
            testsuite.setCore(suiteEntity.getCore());
            testsuite.setAggregated(suiteEntity.getAggregated());
            if(suiteEntity.getTestcases() != null){
                testsuite.setTestcount(suiteEntity.getTestcases().size());
            }
        }
        return testsuite;
    }

    private Testcase getTestcase(TestcaseEntity testcaseEntity){
        Testcase testcase = null;
        if(testcaseEntity != null) {
            testcase = new Testcase();
            testcase.setCreatedBy(testcaseEntity.getCreatedBy());
            testcase.setTestcaseMethodName(testcaseEntity.getTestcase_id());
            testcase.setCreatedOn(testcaseEntity.getCreatedOn());
            testcase.setId(testcaseEntity.getId());
            testcase.setLastModifiedOn(testcaseEntity.getLastModifiedOn());
            testcase.setVersion(testcaseEntity.getVersion());
            testcase.setTestsuite_id(testcaseEntity.getId());
            testcase.setDescription(testcaseEntity.getDescription());
            testcase.setEnabled(testcaseEntity.getEnabled());
            testcase.setTestsuite_id(testcaseEntity.getId());
            testcase.setismanual(testcaseEntity.getIsManual());
            testcase.setGroups(testcaseEntity.getGroups());
            if(testcaseEntity.getTestsuite() != null){
                testcase.setTestsuitename(testcaseEntity.getTestsuite().getTestsuitename());
            }
        }
        return testcase;

    }

    private TestRun getTestrun(TestrunEntity testRunEntity){
        TestRun testrun = null;
        if(testRunEntity != null){
            testrun = new TestRun();
            testrun.setCreatedBy(testRunEntity.getCreatedBy());
            testrun.setCreatedOn(testRunEntity.getCreatedOn());
            testrun.setId(testRunEntity.getId());
            if(!TEST_STATUS.RUNNING.equals(testRunEntity.getTeststatus())){
                testrun.setEndedOn(testRunEntity.getLastModifiedOn());
            }
            testrun.setLastModifiedOn(testRunEntity.getLastModifiedOn());
            if(testRunEntity.getTeststatus() != null){
                testrun.setStatus(testRunEntity.getTeststatus().toString());
            }
            testrun.setTestsuiteidfk(testRunEntity.getTestsuiteid());
            testrun.setVersion(testRunEntity.getVersion());
            testrun.setEnvironment(testRunEntity.getEnvironment());
            testrun.setTotalPassed(testRunEntity.getPassed());
            testrun.setTotalFailed(testRunEntity.getFailed());
            testrun.setTotalSkipped(testRunEntity.getSkipped());
            testrun.setTotalTests(testRunEntity.getTotaltests());
            testrun.setTotalManual(testRunEntity.getManual());
            testrun.setFailure(testRunEntity.getFailure());
            testrun.setBranch(testRunEntity.getBranch());
            if(testRunEntity.getTestsuite() != null){
                testrun.setSuitename(testRunEntity.getTestsuite().getTestsuitename());
            }

        }
        return testrun;

    }


    private TestResult getTestResult(TestresultEntity testresultE){
        TestResult testResult = null;
        if(testresultE != null){
            testResult = new TestResult();
            testResult.setCreatedBy(testresultE.getCreatedBy());
            testResult.setCreatedOn(testresultE.getCreatedOn());
            testResult.setError(testresultE.getError());
            testResult.setErrormessage(testresultE.getErrorMessage());
            testResult.setId(testresultE.getId());
            testResult.setLastModifiedOn(testresultE.getLastModifiedOn());
            testResult.setResult(testresultE.getResult());
            testResult.setTestcaseid(testresultE.getTestcaseid());
            testResult.setTestrunid(testresultE.getTestrunid());
            testResult.setVersion(testresultE.getVersion());
            testResult.setStarttime(testresultE.getStarttime());
            testResult.setEndtime(testresultE.getEndtime());
            testResult.setTestmethodname(testresultE.getTestcasemethodname());
            testResult.setTestcasedescription(testresultE.getTestcasedescription());
            testResult.setTestparams(testresultE.getTestparams());
            testResult.setGroups(testresultE.getGroups());

        }

        return testResult;

    }

    @Override
    public List<Testsuite> getAllSuites() {
        List<Testsuite> testsuite = new ArrayList<Testsuite>();
        Iterable<TestsuiteEntity> testsuites = testSuiteRepository.findAll();
        for(TestsuiteEntity testSuiteE:testsuites){
            testsuite.add(getTestSuite(testSuiteE));
        }
        return testsuite;
    }

    public boolean isInited() {
        return inited;
    }

    public void setInited(boolean inited) {
        this.inited = inited;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @Override
    public Testsuite getTestsuiteBySuiteName(String suitename) {
        TestsuiteEntity suiteEntity = testSuiteRepository.findByTestsuitenameIgnoreCase(suitename);

        return getTestSuite(suiteEntity);
    }

    @Override
    public List<TestResult> getTestresult(Long testcaseid, Integer count, Integer page) {
        List<TestResult> testResults = new ArrayList<TestResult>();
        Testcase testcase = getTestcaseByTestId(testcaseid);
        if(testcase != null){
            page = (page == null || page < new Integer(0) ) ? new Integer(0): page;
            PageRequest request =
                    new PageRequest(page, count, Sort.Direction.DESC, "createdOn");

            Page<TestresultEntity> testcaseEs = testResultRepository.findByTestcaseid(testcaseid,request);
            if(testcase != null){
                for(TestresultEntity testresultE: testcaseEs){
                    testResults.add(getTestResult(testresultE));
                }
            }
        }
        return testResults;
    }

    @Override
    public List<TestResult> getTestResultByRunIdStatus(Long runId, Integer status) {
        List<TestResult> testResults = new ArrayList<TestResult>();
        List<TestresultEntity> testRunEs = testResultRepository.findByTestrunid(runId);
        if(testRunEs != null && !testRunEs.isEmpty()){
            for(TestresultEntity testRunE: testRunEs){
                if(status == testRunE.getResult()){
                    testResults.add(getTestResult(testRunE));
                }
            }
        }
        return testResults;
    }


    @Override
    public TestResult marktestPass(Long resultid, String testcomment, String userid) {
        return updateTestResult(SUCCESS,resultid, testcomment, userid);
    }

    @Override
    public TestResult marktestFail(Long resultid, String testcomment, String userid) {
        return updateTestResult(FAILURE,resultid, testcomment, userid);
    }

    private synchronized TestResult updateTestResult(Integer result, Long resultid, String testcomment, String userid) {
        TestresultEntity testresultE = testResultRepository.getOne(resultid);
        if(result == SUCCESS && testresultE.getResult() == SUCCESS){
            throw new RuntimeException("Test is already passed");
        }else if(result == FAILURE && testresultE.getResult() == FAILURE){
            throw new RuntimeException("Test is already failed");
        }
        Optional<TestcaseEntity> testcaseC = testCaseRepository.findById(testresultE.getTestcaseid());
        TestcaseEntity testcaseE = testcaseC.get();
        boolean isManual = testcaseE.getIsManual();
        testresultE.setComments(testcomment);
        if(userid != null && !userid.isEmpty()){
            testresultE.setUpdatedBy(userid);
        }
        TestrunEntity testRunE = testRunRepository.getOne(testresultE.getTestrunid());
        if(result.equals(SUCCESS) && testRunE.getTotaltests() > testRunE.getPassed() && testresultE.getResult() != SUCCESS){
            if(isManual && testresultE.getResult() == 5){
                testRunE.setManual(testRunE.getManual() - 1);
            }else if(testresultE.getResult() == SKIP){
                testRunE.setSkipped(testRunE.getSkipped() - 1);
            }
            else{
                testRunE.setFailed(testRunE.getFailed() - 1);
            }
            testRunE.setPassed(testRunE.getPassed() + 1);
        }else if(result.equals(FAILURE) && testresultE.getResult() != FAILURE){
            if(isManual && testresultE.getResult() == 5){
                testRunE.setManual(testRunE.getManual() - 1);
            }else if(testresultE.getResult() == SKIP){
                testRunE.setSkipped(testRunE.getSkipped() - 1);
            }else{
                testRunE.setPassed(testRunE.getPassed() - 1);
            }
            testRunE.setFailed(testRunE.getFailed() + 1);
        }
        testresultE.setResult(result);
        testresultE = testResultRepository.save(testresultE);
        testRunRepository.save(testRunE);
        return getTestResult(testresultE);
    }

    @Override
    public List<Testcase> getAllTestcases() {
        List<Testcase> testcases = new ArrayList<Testcase>();
        List<TestcaseEntity> testcaseEntity = testCaseRepository.findAll();
        if(testcaseEntity != null && testcaseEntity.size() > 0){
            for(TestcaseEntity testcaseE: testcaseEntity){
                testcases.add(getTestcase(testcaseE));
            }
        }

        return null;
    }

    @Override
    public TestRun getTestRunsForRunId(Long runId) {
        TestRun testrun = null;
        TestrunEntity testrunE = testRunRepository.getOne(runId);

        if(testrunE != null){
            TestsuiteEntity suite = testSuiteRepository.findById(testrunE.getTestsuiteid()).get();
            testrun = getTestrun(testrunE);
            testrun.setSuitename(suite.getTestsuitename());
        }
        return testrun;
    }


    @Override
    public List<Team> getTeams(boolean enabled) {
        List<Testsuite> allsuites = getAllSuites();
        List<String> allTeams = testSuiteRepository.allTeams();

        List<Team> teams = new ArrayList<Team>();
        if(allTeams != null && !allTeams.isEmpty()){
            for(String teamname: allTeams){
                if(teamname == null){
                    continue;
                }
                Team team = new Team();
                team.setName(teamname);
                team.setId(new Long(teamname.hashCode()));
                for(Testsuite suite: allsuites){
                    if(suite.getTeam() != null && suite.getTeam().equalsIgnoreCase(teamname)){
                        if(enabled && suite.getEnabled()){
                            team.addTestsuites(suite);
                        }
                    }
                }
                if(!team.getTestsuites().isEmpty()){
                    teams.add(team);
                }
            }
        }
        return teams;
    }

    @Override
    public List<Testcase> getManualTestCases(Long suiteid) {
        List<Testcase> testcases = getTestcasesBySuiteId(suiteid);
        List<Testcase> manualtestcases = new ArrayList<Testcase>();

        for(Testcase testcase:testcases ){
            if(testcase.ismanual()){
                manualtestcases.add(testcase);
            }
        }
        return manualtestcases;
    }

    @Override
    public List<TestRun> getTestRunsForSuiteIdEnv(Long suiteId, Integer count, Integer page, String env) {
        List<TestRun> testruns = new ArrayList<TestRun>();
        Testsuite suite = getTestsuite(suiteId);
        if(suite != null){
            page = (page == null || page < new Integer(0) ) ? new Integer(0): page;
            PageRequest request =
                    new PageRequest(page, count, Sort.Direction.DESC, "id");
            List<TestrunEntity> testResultEs = testRunRepository.findByTestsuiteidAndEnvironment(suite.getId(), env, request);
            if(testResultEs != null && !testResultEs.isEmpty()){
                for(TestrunEntity testResultE: testResultEs){
                    testruns.add(getTestrun(testResultE));
                }
            }
        }
        return testruns;
    }

    @Override
    public Long getTestrunCount() {
        return testRunRepository.count();
    }

    @Override
    public Long getTestresultCount() {
        return testResultRepository.count();
    }

    @Override
    public Long getTestcaseCount() {
        return testCaseRepository.count();
    }

    @Override
    public List<TestConfig> getAllTestconfig() {
        List<TestConfigEntity> testConfigEntitys = testConfigRepository.findAll();
        List<TestConfig> testConfigs = new ArrayList<TestConfig>();
        if(testConfigEntitys != null && testConfigEntitys.size() > 0){
            for(TestConfigEntity testConfigEntity:testConfigEntitys ){
                testConfigs.add(getTestConfig(testConfigEntity));
            }
        }
        return testConfigs;
    }

    @Override
    public TestConfig getTestconfig(Long id) {
        TestConfigEntity testConfigEntity = testConfigRepository.findById(id).get();
        TestConfig testConfig = null;
        if(testConfigEntity != null){
            testConfig = getTestConfig(testConfigEntity);
        }
        return testConfig;
    }

    private TestConfig getTestConfig(TestConfigEntity testConfigEntity) {
        TestConfig testConfig = new TestConfig();
        testConfig.setCreatedBy(testConfigEntity.getCreatedBy());
        testConfig.setCreatedOn(testConfigEntity.getCreatedOn());
        testConfig.setId(testConfigEntity.getId());
        testConfig.setHour(testConfigEntity.getHour());
        testConfig.setMinute(testConfigEntity.getMinute());
        testConfig.setLastModifiedOn(testConfigEntity.getLastModifiedOn());
        testConfig.setStarttime(testConfigEntity.getStarttime());
        testConfig.setVersion(testConfigEntity.getVersion());
        testConfig.setTestsuite_id_fk(testConfigEntity.getTestsuiteid());
        TestsuiteEntity suiteEntity = testSuiteRepository.findById(testConfigEntity.getTestsuiteid()).get();
        testConfig.setTestsuitename(suiteEntity.getTestsuitename());
        testConfig.setEnvironment(testConfigEntity.getEnvironment());

        if(testConfigEntity.getTestconfigInfos() != null && testConfigEntity.getTestconfigInfos().size() > 0){
            for(TestConfigInfoEntity testConfigInfoEntity:testConfigEntity.getTestconfigInfos() ){

                TestConfigInfo testConfigInfo = new TestConfigInfo();
                testConfigInfo.setType(testConfigInfoEntity.getType());
                testConfigInfo.setEnabled(testConfigInfoEntity.getEnabled());
                testConfigInfo.setValue(testConfigInfoEntity.getValue());
                testConfigInfo.setCreatedBy(testConfigInfoEntity.getCreatedBy());
                testConfigInfo.setCreatedOn(testConfigInfoEntity.getCreatedOn());
                testConfigInfo.setId(testConfigInfoEntity.getId());
                testConfigInfo.setLastModifiedOn(testConfigInfoEntity.getLastModifiedOn());
                testConfigInfo.setVersion(testConfigInfoEntity.getVersion());
                testConfig.getTestconfiginfos().add(testConfigInfo);
            }
        }
        return testConfig;
    }

    @Override
    public TestConfig addUpdateTestConfig(TestConfig testConfig) {

        if(testConfig.getStarttime() == null){
            testConfig.setStarttime(new Date());
        }

        TestConfigEntity testConfigEntity = getTestConfigEntity(testConfig);

        // This will not execute when adding testconfig only
        if(testConfig.getTestconfiginfos() != null && !testConfig.getTestconfiginfos().isEmpty()){
            for(TestConfigInfo testConfigInfo:testConfig.getTestconfiginfos() ){
                TestConfigInfoEntity testconfigEntityInfo = null;
                if(testConfigInfo.getId() != null){
                    testconfigEntityInfo = testConfigInfoRepository.findById(testConfigInfo.getId()).get();
                }else{
                    testconfigEntityInfo = new TestConfigInfoEntity();
                }

                testconfigEntityInfo.setEnabled(testConfigInfo.getEnabled());
                testconfigEntityInfo.setTestconfig(testConfig.getId());
                testconfigEntityInfo.setCreatedBy(testConfig.getCreatedBy());
                testconfigEntityInfo.setValue(testConfigInfo.getValue());
                testconfigEntityInfo.setType(testConfigInfo.getType());
                testConfigInfoRepository.saveAndFlush(testconfigEntityInfo);
            }
        }

        testConfigRepository.saveAndFlush(testConfigEntity);
        return getTestConfig(testConfigEntity);
    }


    private TestConfigEntity getTestConfigEntity(TestConfig testConfig){
        TestConfigEntity testConfigEntity;

        if(testConfig.getId() != null && testConfigRepository.existsById(testConfig.getId())){
            testConfigEntity = testConfigRepository.findById(testConfig.getId()).get();
        }else{
            testConfigEntity = new TestConfigEntity();
        }

        testConfigEntity.setEnvironment(testConfig.getEnvironment());
        testConfigEntity.setCreatedOn(testConfig.getCreatedOn());
        testConfigEntity.setStarttime(testConfig.getStarttime());
        testConfigEntity.setLastModifiedOn(testConfig.getLastModifiedOn());
        testConfigEntity.setId(testConfig.getId());
        testConfigEntity.setHour(testConfig.getHour());
        testConfigEntity.setMinute(testConfig.getMinute());
        testConfigEntity.setVersion(testConfig.getVersion());
        testConfigEntity.setCreatedBy(testConfig.getCreatedBy());
        testConfigEntity.setTestsuiteid(testConfig.getTestsuite_id_fk());
        return testConfigEntity;
    }

    @Override
    public List<Environment> getEnvironments() {
        List<Environment> envs = new ArrayList<Environment>();
        List<EnvEntity> envEntities = envRepository.findAll();
        if(envEntities != null && !envEntities.isEmpty()){
            for(EnvEntity envEntity: envEntities){
                envs.add(getEnvironment(envEntity));
            }
        }

        return envs;
    }

    private Environment getEnvironment(EnvEntity envEntity) {
        Environment env = new Environment();
        env.setCreatedBy(envEntity.getCreatedBy());
        env.setCreatedOn(envEntity.getCreatedOn());
        env.setEnvironment(envEntity.getEnvironment());
        env.setKey(envEntity.getKey());
        env.setId(envEntity.getId());
        env.setLastModifiedOn(envEntity.getLastModifiedOn());
        env.setVersion(envEntity.getVersion());
        return env;

    }

    @Override
    public void addEnvironments(String environment) {
        List<EnvEntity> envlist = envRepository.findAll();
        if(envlist != null && !envlist.isEmpty()){
            for(EnvEntity envE:envlist){
                if(envE.getEnvironment().toLowerCase().equals(environment.toLowerCase())){
                    return;
                }
            }
        }

        EnvEntity envE = new EnvEntity();
        envE.setKey(environment.toLowerCase());
        envE.setEnvironment(environment.toLowerCase());
        envRepository.save(envE);
    }


}