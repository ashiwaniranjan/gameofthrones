package framework.gameofthrones.Targaryen.domain;

import framework.gameofthrones.Targaryen.entity.TestConfigInfoEntity;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "testconfig")
public class TestConfig  extends BaseEntry{

	private static final long serialVersionUID = 550285197906819066L;
	
	private List<TestConfigInfo> testconfiginfos = new ArrayList<TestConfigInfo>();
	private Long testsuite_id_fk;
	private String testsuitename;
	private String environment;
	private Date starttime;
	private Integer hour;
	private Integer minute;
	private Date nextrun;
	
	private List<TestConfigInfoEntity.TYPE> options = Arrays.asList(TestConfigInfoEntity.TYPE.values());

	public List<TestConfigInfo> getTestconfiginfos() {
		return testconfiginfos;
	}

	public void setTestconfiginfos(List<TestConfigInfo> testconfiginfos) {
		this.testconfiginfos = testconfiginfos;
	}

	public Long getTestsuite_id_fk() {
		return testsuite_id_fk;
	}

	public void setTestsuite_id_fk(Long testsuite_id_fk) {
		this.testsuite_id_fk = testsuite_id_fk;
	}

	public String getTestsuitename() {
		return testsuitename;
	}

	public void setTestsuitename(String testsuitename) {
		this.testsuitename = testsuitename;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public List<TestConfigInfoEntity.TYPE> getOptions() {
		return options;
	}

	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getMinute() {
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}

	public Date getNextrun() {
		Calendar now = Calendar.getInstance();
		Calendar nextSchedule = Calendar.getInstance();
		nextSchedule.setTimeInMillis(starttime.getTime());
		while(nextSchedule.compareTo(now) < 0){
			nextSchedule.add(Calendar.HOUR, hour);
			nextSchedule.add(Calendar.MINUTE, minute);
		}
		
		nextrun = new Date(nextSchedule.getTimeInMillis());
		
		return nextrun;
	}

}
