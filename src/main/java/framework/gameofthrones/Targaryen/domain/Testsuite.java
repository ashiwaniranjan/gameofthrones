package framework.gameofthrones.Targaryen.domain;


import org.apache.commons.lang3.time.DateUtils;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;


/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "testsuite")
public class Testsuite extends BaseEntry{

	private static final long serialVersionUID = -7148557588900308183L;
	
	private String suitename;
	private Boolean enabled;
	private int testcount;
	private int progress;
	private boolean isrecentlyrun;
	private String team;
	private String core;
	private String type;
	private boolean aggregated;


	public boolean isrecentlyrun() {
        isrecentlyrun = DateUtils.addDays(getLastModifiedOn(), 2).after(new Date());
		return isrecentlyrun;
	}

	public String getSuitename() {
		return suitename;
	}

	public void setSuitename(String suitename) {
		this.suitename = suitename;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public int getTestcount() {
		return testcount;
	}

	public void setTestcount(int testcount) {
		this.testcount = testcount;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getCore() {
		return core;
	}

	public void setCore(String core) {
		this.core = core;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAggregated() {
		return aggregated;
	}

	public void setAggregated(boolean aggregated) {
		this.aggregated = aggregated;
	}
}
