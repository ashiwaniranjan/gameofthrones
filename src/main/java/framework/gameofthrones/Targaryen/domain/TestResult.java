package framework.gameofthrones.Targaryen.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "testresult")
public class TestResult extends BaseEntry{

	private static final long serialVersionUID = 393561536446756645L;
	
	private Long testcaseid;
	private int result;
	private String error;
	private String errormessage;
	private Long testrunid;
	private Date starttime;
	private Date endtime;
	private String testmethodname;
	private String testcasedescription;
	private String testcomment;
	private String testparams;
	private String groups;
	
	public Long getTestcaseid() {
		return testcaseid;
	}
	public void setTestcaseid(Long testcaseid) {
		this.testcaseid = testcaseid;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getErrormessage() {
		return errormessage;
	}
	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public Long getTestrunid() {
		return testrunid;
	}
	public void setTestrunid(Long testrunid) {
		this.testrunid = testrunid;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	public String getTestmethodname() {
		return testmethodname;
	}
	public void setTestmethodname(String testmethodname) {
		this.testmethodname = testmethodname;
	}
	public String getTestcasedescription() {
		return testcasedescription;
	}
	public void setTestcasedescription(String testcasedescription) {
		this.testcasedescription = testcasedescription;
	}
	public String getTestcomment() {
		return testcomment;
	}
	public void setTestcomment(String testcomment) {
		this.testcomment = testcomment;
	}
	public String getTestparams() {
		return testparams;
	}
	public void setTestparams(String testparams) {
		this.testparams = testparams;
	}
	public String getGroups() {
		return groups;
	}
	public void setGroups(String groups) {
		this.groups = groups;
	}

	
}
