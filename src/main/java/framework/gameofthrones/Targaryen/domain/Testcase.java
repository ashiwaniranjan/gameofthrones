package framework.gameofthrones.Targaryen.domain;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "testcase")
public class Testcase extends BaseEntry{

	private static final long serialVersionUID = -7074615603199314050L;

	private String testcaseid;
	private Long testsuiteid;
	private boolean enabled;
	private String description;
	private String testsuitename;
	private String testparams;
	private boolean ismanual;
	private String groups;
	
	public String getTestcaseMethodName() {
		return testcaseid;
	}
	public void setTestcaseMethodName(String testcase_id) {
		this.testcaseid = testcase_id;
	}
	public Long getTestsuite_id() {
		return testsuiteid;
	}
	public void setTestsuite_id(Long testsuite_id) {
		this.testsuiteid = testsuite_id;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTestsuitename() {
		return testsuitename;
	}
	public void setTestsuitename(String testsuitename) {
		this.testsuitename = testsuitename;
	}
	public String getTestparams() {
		return testparams;
	}
	public void setTestparams(String testparams) {
		this.testparams = testparams;
	}
	public boolean ismanual() {
		return ismanual;
	}
	public void setismanual(boolean ismanual) {
		this.ismanual = ismanual;
	}
	public String getGroups() {
		return groups;
	}
	public void setGroups(String groups) {
		this.groups = groups;
	}
	

}
