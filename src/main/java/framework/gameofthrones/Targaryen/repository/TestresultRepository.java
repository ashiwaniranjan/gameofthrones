package framework.gameofthrones.Targaryen.repository;


import framework.gameofthrones.Targaryen.entity.TestresultEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="testresultcache")
public interface TestresultRepository extends JpaRepository<TestresultEntity, Long>{
	
	String FIND_BY_RUNID_QUERY = "Select trs.*, tc.testcase_id, tc.description, tc.groups from testresult trs inner join testcase tc on trs.testcase_id_fk = tc.id and trs.testrun_id_fk =  :testrunid order by trs.created_on desc";
	
	List<TestresultEntity> findByTestcaseidfk(Long testcaseidfk);
	
	@Cacheable
	Page<TestresultEntity> findByTestcaseid(Long testcaseid, Pageable pageRequest);
	
	@Cacheable
	@Query(value=FIND_BY_RUNID_QUERY,nativeQuery = true)
	List<TestresultEntity> findByTestrunid(@Param("testrunid") Long testrunid);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestresultEntity> S save(S entities);
	
	@Override
	@Cacheable
	long count();
	
}
