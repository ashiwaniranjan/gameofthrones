package framework.gameofthrones.Targaryen.repository;



import framework.gameofthrones.Targaryen.entity.TestConfigEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="testconfigcache")
public interface TestconfigRepository extends JpaRepository<TestConfigEntity, Long>{

	@Cacheable(key="#p0")
	@Override
	Optional<TestConfigEntity> findById(Long id);
	
	@Cacheable
	@Override
	List<TestConfigEntity> findAll();
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestConfigEntity> S save(S entities);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestConfigEntity> S saveAndFlush(S entity);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	void delete(TestConfigEntity entity);
	
}
