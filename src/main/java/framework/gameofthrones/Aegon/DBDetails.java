package framework.gameofthrones.Aegon;

import framework.gameofthrones.Tyrion.SqlTemplate;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

/**
 * @author abhijit.p
 * @since 20 Jan 2018
**/
public interface DBDetails {

    DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
    SqlTemplate getDatabaseService(String dbName);
}
