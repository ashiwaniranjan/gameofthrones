package framework.gameofthrones.Aegon;


import com.google.common.base.Function;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static framework.gameofthrones.Aegon.ElementIdentificationType.APPIUMDRIVER;


public class ElementLocator
{
    private final AppiumDriver appiumDriver;
    private final WebDriver webDriver;
    public By by;
    public String accId;
    private  ElementIdentificationType ElementIdentificationType;
    private IElement cachedElement;
    private List<IElement> cachedElementList;
    private final String SPLITTER = "@#@";

    public ElementLocator(AppiumDriver androidDriver ,Field field,InitializeUI init)
    {
       // System.out.println("androidDriver = "+androidDriver);
        ElementIdentificationType type = null;
        String data = "";
        this.appiumDriver = androidDriver;
        this.webDriver=null;
        this.by=by;

        this.accId=accId;
        ElementIdentificationType = APPIUMDRIVER;

        System.out.println(field
                .getDeclaringClass().getSimpleName()
                + "."
                + field.getName());
        UIElement type1 = init.UIObjects.GetUIElementDetails(field
                .getDeclaringClass().getSimpleName()
                + "."
                + field.getName());

        //System.out.println(type1.type);
        switch (type1.type) {

            case APPIUMDRIVER:
                ElementIdentificationType = APPIUMDRIVER;

                if(init.platformnew.equals("Android")) {
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).andoridValue;
                }
                if(init.platformnew.equals("ios")){
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).iOsValue;

                }

                switch (data.split(SPLITTER)[0]) {
                    case "xpath":
                    	System.out.println(data.split(SPLITTER)[1]);
                        by = By.xpath(data.split(SPLITTER)[1]);
                        break;
                    case "id":
                        by = By.id(data.split(SPLITTER)[1]);
                        break;
                    case "className":
                        by = By.className(data.split(SPLITTER)[1]);
                        break;
                    case "name":
                        by = By.name(data.split(SPLITTER)[1]);
                        break;
                    case "linkText":
                        by = By.linkText(data.split(SPLITTER)[1]);
                        break;
                    case "cssSelector":
                        by = By.cssSelector(data.split(SPLITTER)[1]);
                        break;
                    case "tagName":
                        by = By.tagName(data.split(SPLITTER)[1]);
                        break;
                    case "accessbilityId":
                        accId=data.split(SPLITTER)[1];
                        break;



                    default:

                }
                break;
            case WEBDRIVER:
                ElementIdentificationType = framework.gameofthrones.Aegon.ElementIdentificationType.WEBDRIVER;

                if(init.platformnew.equalsIgnoreCase("mweb")) {
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).mWebValue;
                }
                if(init.platformnew.equalsIgnoreCase("dweb")){
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).dWebValue;

                }

                switch (data.split(SPLITTER)[0]) {
                    case "xpath":
                        by = By.xpath(data.split(SPLITTER)[1]);
                        break;
                    case "id":
                        by = By.id(data.split(SPLITTER)[1]);
                        break;
                    case "className":
                        by = By.className(data.split(SPLITTER)[1]);
                        break;
                    case "name":
                        by = By.name(data.split(SPLITTER)[1]);
                        break;
                    case "linkText":
                        by = By.linkText(data.split(SPLITTER)[1]);
                        break;
                    case "cssSelector":
                        by = By.cssSelector(data.split(SPLITTER)[1]);
                        break;
                    case "tagName":
                        by = By.tagName(data.split(SPLITTER)[1]);
                        break;
                    default:

                }
                break;

        }



    }
    public ElementLocator(WebDriver driver ,Field field,InitializeUI init)
    {
        // System.out.println("androidDriver = "+androidDriver);
        ElementIdentificationType type = null;
        String data = "";
        this.appiumDriver = null;
        this.webDriver=driver;
        this.by=by;

        this.accId=accId;
        ElementIdentificationType = framework.gameofthrones.Aegon.ElementIdentificationType.WEBDRIVER;

        System.out.println(field
                .getDeclaringClass().getSimpleName()
                + "."
                + field.getName());
        UIElement type1 = init.UIObjects.GetUIElementDetails(field
                .getDeclaringClass().getSimpleName()
                + "."
                + field.getName());

        //System.out.println(type1.type);
        switch (type1.type) {

            case APPIUMDRIVER:
                ElementIdentificationType = APPIUMDRIVER;

                if(init.platformnew.equals("Android")) {
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).andoridValue;
                }
                if(init.platformnew.equals("ios")){
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).iOsValue;

                }

                switch (data.split(SPLITTER)[0]) {
                    case "xpath":
                        System.out.println(data.split(SPLITTER)[1]);
                        by = By.xpath(data.split(SPLITTER)[1]);
                        break;
                    case "id":
                        by = By.id(data.split(SPLITTER)[1]);
                        break;
                    case "className":
                        by = By.className(data.split(SPLITTER)[1]);
                        break;
                    case "name":
                        by = By.name(data.split(SPLITTER)[1]);
                        break;
                    case "linkText":
                        by = By.linkText(data.split(SPLITTER)[1]);
                        break;
                    case "cssSelector":
                        by = By.cssSelector(data.split(SPLITTER)[1]);
                        break;
                    case "tagName":
                        by = By.tagName(data.split(SPLITTER)[1]);
                        break;
                    case "accessbilityId":
                        accId=data.split(SPLITTER)[1];
                        break;



                    default:

                }
                break;
            case WEBDRIVER:
                ElementIdentificationType = framework.gameofthrones.Aegon.ElementIdentificationType.WEBDRIVER;

                if(init.platformnew.equalsIgnoreCase("mweb")) {
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).mWebValue;
                }
                if(init.platformnew.equalsIgnoreCase("dweb")){
                    data = init.UIObjects.GetUIElementDetails(field
                            .getDeclaringClass().getSimpleName()
                            + "."
                            + field.getName()).dWebValue;

                }

                switch (data.split(SPLITTER)[0]) {
                    case "xpath":
                        by = By.xpath(data.split(SPLITTER)[1]);
                        break;
                    case "id":
                        by = By.id(data.split(SPLITTER)[1]);
                        break;
                    case "className":
                        by = By.className(data.split(SPLITTER)[1]);
                        break;
                    case "name":
                        by = By.name(data.split(SPLITTER)[1]);
                        break;
                    case "linkText":
                        by = By.linkText(data.split(SPLITTER)[1]);
                        break;
                    case "cssSelector":
                        by = By.cssSelector(data.split(SPLITTER)[1]);
                        break;
                    case "tagName":
                        by = By.tagName(data.split(SPLITTER)[1]);
                        break;
                    default:

                }
                break;

        }



    }


    public IElement findElement(int findTimeout, int visbilityTimeout)
    {
        youtubeElement element = null;

        switch (ElementIdentificationType)
        {
            case APPIUMDRIVER:
                //System.out.println("***New Element " + by.toString());
               MobileElement webElement = new FluentWait<WebDriver>(appiumDriver)
                        .withTimeout(findTimeout, TimeUnit.SECONDS)
                        .pollingEvery(500, TimeUnit.MILLISECONDS)
                        .ignoring(NoSuchElementException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .withMessage(
                                "Element could not be found. Finder - ")
                        .until(new Function<WebDriver, MobileElement>()
                        {
                            public MobileElement apply(WebDriver driver)
                            {
                                //System.out.println("Searching for element - "+ by.toString());
                                driver = appiumDriver;
                                if(by!=null){
                                return (MobileElement)driver.findElement(by);}
                                else{
                                    return (MobileElement) ((AndroidDriver) driver).findElementByAccessibilityId(accId);
                                }
                            }
                        });
                new FluentWait<WebElement>(webElement)
                        .withTimeout(visbilityTimeout, TimeUnit.SECONDS)
                        .pollingEvery(500, TimeUnit.MILLISECONDS)
                        .ignoring(WebDriverException.class)
                        .withMessage(
                                "Element was not visible. Finder - "
                                        )
                        .until(new Function<WebElement, Boolean>()
                        {
                            public Boolean apply(WebElement webElement)
                            {
                                //System.out.println("Checking if element is displayed - "+ by.toString());
                                return webElement.isDisplayed();
                            }
                        });
                element = new youtubeElement(appiumDriver, webElement);
                break;

            case WEBDRIVER:
                System.out.println(by.toString());
                WebElement webElement_web = new FluentWait<WebDriver>(webDriver)
                        .withTimeout(findTimeout, TimeUnit.SECONDS)
                        .pollingEvery(500, TimeUnit.MILLISECONDS)
                        .ignoring(NoSuchElementException.class)
                        .ignoring(ElementNotVisibleException.class)
                        .withMessage(
                                "Element could not be found. Finder - "+by.toString())
                        .until(new Function<WebDriver, WebElement>()
                        {
                            public WebElement apply(WebDriver driver)
                            {
                                //System.out.println("Searching for element - "+ by.toString());
                                driver = webDriver;
                                return driver.findElement(by);
                            }
                        });
                new FluentWait<WebElement>(webElement_web)
                        .withTimeout(visbilityTimeout, TimeUnit.SECONDS)
                        .pollingEvery(500, TimeUnit.MILLISECONDS)
                        .ignoring(WebDriverException.class)
                        .withMessage(
                                "Element was not visible. Finder - ")
                        .until(new Function<WebElement, Boolean>()
                        {
                            public Boolean apply(WebElement webElement)
                            {
                                //System.out.println("Checking if element is displayed - "+ by.toString());
                                return webElement.isDisplayed();
                            }
                        });
                element = new youtubeElement(webDriver, webElement_web);


        }
        System.out.println("Element found");
        return element;
    }

    public List<IElement> findElements(int findTimeout) {
        List<IElement> elements = new ArrayList<IElement>();
        List<MobileElement> webElementList;
        List<WebElement> webElementListNew;

        youtubeElement element = null;


        //System.out.println(by);
        switch (ElementIdentificationType) {
            case APPIUMDRIVER:
                webElementList = new FluentWait<AppiumDriver>(appiumDriver)
                        .withTimeout(findTimeout, TimeUnit.SECONDS)
                        .pollingEvery(500, TimeUnit.MILLISECONDS)
                        .ignoring(NoSuchElementException.class)
                        .withMessage(
                                "Element could not be found - ")
                        .until(new Function<AppiumDriver, List<MobileElement>>() {
                            public List<MobileElement> apply(AppiumDriver driver) {
                                //System.out.println("Searching for list - "+ by.toString());
                                driver = appiumDriver;
                                if(by!=null){
                                    return (List<MobileElement>)driver.findElements(by);}
                                else{
                                    return (List<MobileElement>)driver.findElementsByAccessibilityId(accId);
                                }


                            }
                        });
                        for (WebElement singleWebElement : webElementList) {
                             elements.add(new youtubeElement(webDriver, singleWebElement));
                          }


                break;
            case WEBDRIVER:
                webElementListNew = new FluentWait<WebDriver>(webDriver)
                        .withTimeout(findTimeout, TimeUnit.SECONDS)
                        .pollingEvery(500, TimeUnit.MILLISECONDS)
                        .ignoring(NoSuchElementException.class)
                        .withMessage(
                                "Element could not be found - " + by.toString())
                        .until(new Function<WebDriver, List<WebElement>>() {
                            public List<WebElement> apply(WebDriver driver) {
                                //System.out.println("Searching for list - "+ by.toString());
                                driver = webDriver;

                                return driver.findElements(by);
                            }
                        });
                for (WebElement singleWebElement : webElementListNew) {
                    elements.add(new youtubeElement(webDriver, singleWebElement));
                }


            break;


        }
        return elements;
    }




}

