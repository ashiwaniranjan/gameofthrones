package framework.gameofthrones.hudor;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class HodorHelper {

    Initialize gameofthrones = new Initialize();

    public String hodorUpdateResult(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("result", "createresult", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        String runid = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.testrun_id").replace("[", "").replace("]", "").replace("\"", "").trim();return runid;
    }
    private HashMap<String, String> getDefaultHeader() {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        return requestHeader;
    }
}
