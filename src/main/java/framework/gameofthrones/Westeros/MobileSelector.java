package framework.gameofthrones.Westeros;


    public enum MobileSelector {
        ACCESSIBILITY("accessibility id"),
        ANDROID_UI_AUTOMATOR("-android uiautomator"),
        IOS_UI_AUTOMATION("-ios uiautomation"),
        IOS_PREDICATE_STRING("-ios predicate string"),
        IOS_CLASS_CHAIN("-ios class chain"),
        WINDOWS_UI_AUTOMATION("-windows uiautomation"),
        IMAGE("-image"),
        ANDROID_VIEWTAG("-android viewtag"),
        CUSTOM("-custom");

        private final String selector;

        MobileSelector(String selector) {
            this.selector = selector;
        }


        public String toString() {
            return this.selector;
        }
    }


