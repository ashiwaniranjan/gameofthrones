package framework.gameofthrones.Westeros;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ElementLocator< T extends WebDriver > extends LocatorCreator {

    private T webDriver;
    private Locator locator;
    private final FindElementFactory factory = new FindElementFactory();


    public ElementLocator( T driver, Field field) {
        super(field);
        this.webDriver = driver;
        this.locator = super.locator;
    }


    public IElement findElement(long findTimeout, long pollingTime) {
        return new BaseElement( webDriver , factory.findElement( webDriver, findTimeout , pollingTime, locator ) );
    }

    public List<IElement> findElements(int findTimeout) {
        List<IElement> elements = new ArrayList<>();
        List< ? extends WebElement> webElements;
        webElements = factory.findElements( webDriver, findTimeout, 500, locator);
        elements.addAll( webElements.stream().map(androidElement -> androidElement != null  ? new BaseElement( webDriver , androidElement) : null).collect(Collectors.toList()));
        return elements;
    }
}


