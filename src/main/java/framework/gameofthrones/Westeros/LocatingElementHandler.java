package framework.gameofthrones.Westeros;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class LocatingElementHandler implements InvocationHandler {
    private final ElementLocator elementLocator;
    private final int DEFAULT_FIND_TIMEOUT = 20;
    private final long DEFAULT_POLLING_TIME = 800;
    private long PAGE_TIMEOUT ; // TODO: 2019-07-23  


    public LocatingElementHandler(ElementLocator ElementLocator) {
        this.elementLocator = ElementLocator;
    }

    @Override
    public Object invoke(Object proxy, final Method method,
                         final Object[] objects) throws Throwable {
        IElement element = this.elementLocator.findElement( DEFAULT_FIND_TIMEOUT, DEFAULT_POLLING_TIME );

        if ("getWrappedElement".equals(method.getName()))
            return element;

        try {
            return method.invoke(element, objects);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }
}