package framework.gameofthrones.Westeros;

import com.google.api.client.util.Preconditions;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

public class UiLauncher {

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Map<String, String> baseCapabilities;
    private JsonReader reader;
    private AppiumDriver driver;
    static String platformName;
    private static final Properties properties;
    static final String resourceName;
    String platformVersion;
    String deviceID;
    String deviceName;
    String systemPort;
    String appActivity;
    String appPackage;
    static String app;
    String chromeDriverPort;
    String wdaLocalPort;
    String webkitDebugProxyPort;
    DesiredCapabilities capabilities;
    String port;
    CoreRequestor requestor = new CoreRequestor();

    static {
        properties = System.getProperties();
        platformName = properties.getProperty(FrameworkConstants.PLATFORM_NAME);
        resourceName = properties.getProperty(FrameworkConstants.RESOURCE_NAME);
        app = properties.getProperty(FrameworkConstants.APP_PATH);

    }

    /**
     * Default constructor for appium start.
     * Properties are read from maven.
     */

    public UiLauncher() throws MalformedURLException {

        this.appActivity = properties.getProperty( FrameworkConstants.APP_ACTIVITY );
        this.appPackage = properties.getProperty( FrameworkConstants.PACKAGE_NAME );
        setUpDriver(buildConfig(platformName));
    }

    /**
     * @param appActivity Launchable app activity for Android. In case of iOS it might be null value.
     * @param packageName package name / bundle of the iOS / Android test app.
     */
    UiLauncher(String appActivity, String packageName) {

        this.appPackage = packageName;
        this.appActivity = Preconditions.checkNotNull(appActivity);

    }

    void setUpDriver(DesiredCapabilities capabilities) throws MalformedURLException {

        switch (platformName.toLowerCase()) {

            case "android":
                this.driver = new AndroidDriver(new URL(FrameworkConstants.BASE_URL + port + FrameworkConstants.WD_PATH), capabilities);
                break;
            case "ios":
                this.driver = new IOSDriver(new URL(FrameworkConstants.BASE_URL + port + FrameworkConstants.WD_PATH), capabilities);
        }

    }


    private Map<String, String> capabilities(String platform) {

        switch (platform.toLowerCase()) {

            case "android":
                this.baseCapabilities = deSerialise(FrameworkConstants.ANDROID_FILE);
                break;
            case "ios":
                this.baseCapabilities = deSerialise(FrameworkConstants.IOS_FILE);
                break;
            default:
        }

        return this.baseCapabilities;
    }


    private Map<String, String> deSerialise(String file) {

        Type dc = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> caps = gson.fromJson(readJson(file), dc);
        return caps;

    }


    private JsonReader readJson(String file) {
        reader = null;
        try {
            reader = new JsonReader(new FileReader(FrameworkConstants.CONFIG_PATH + file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return reader;
    }


    /**
     * @param platform platform for which capabilities to be built.
     * @return Desired Capabilities for the given @platform.
     */
    DesiredCapabilities buildConfig(String platform) {
        this.capabilities = new DesiredCapabilities(capabilities(platform));
        capabilities.setCapability(FrameworkConstants.DEVICE_ID, deviceID = deviceID != null ? this.deviceID : ""); // Specific to RobusTest
        //capabilities.setCapability(FrameworkConstants.UDID, deviceID = deviceID != null ? this.deviceID : ""); // It will be used in local mapping of udid.

        switch (platform) {

            case "android":
                capabilities.setCapability(FrameworkConstants.APP_PACKAGE, this.appPackage);
                capabilities.setCapability(FrameworkConstants.DEVICE_NAME, deviceName = deviceName != null ? this.deviceName : FrameworkConstants.DEFAULT_DEVICENAME);
                capabilities.setCapability(FrameworkConstants.SYSTEM_PORT, systemPort = systemPort != null ? this.systemPort : FrameworkConstants.DEFAULT_SYSTEM_PORT);
                capabilities.setCapability(FrameworkConstants.APP_ACTIVITY, this.appActivity);
                capabilities.setCapability(FrameworkConstants.APP, getUrlFromCloud());
                // capabilities.setCapability(FrameworkConstants.APP, app);
                capabilities.setCapability(FrameworkConstants.CHROME_DRIVER_PORT, chromeDriverPort = chromeDriverPort != null ? this.chromeDriverPort : FrameworkConstants.DEFAULT_CHROMEDRIVER_PORT);
                break;
            case "ios":
                capabilities.setCapability(FrameworkConstants.OS_VERSION, this.platformVersion = this.platformVersion != null ? this.platformVersion : new String());
                capabilities.setCapability(FrameworkConstants.BUNDLE_ID, this.appPackage);
                capabilities.setCapability(FrameworkConstants.WDPP, webkitDebugProxyPort = webkitDebugProxyPort != null ? this.webkitDebugProxyPort : FrameworkConstants.DEFAULT_WEBKITLOCALPROXY);
                capabilities.setCapability(FrameworkConstants.WDA_LOCAL_PORT, wdaLocalPort = wdaLocalPort != null ? this.wdaLocalPort : FrameworkConstants.DEFAULT_WDALOCALPORT);
                break;
        }

        return capabilities;

    }

    public AppiumDriver getDriver() {
        return this.driver;
    }

    public void setCapabilities(DesiredCapabilities capabilities) {
        this.capabilities = capabilities;
    }

    public DesiredCapabilities getCapabilities() {
        return capabilities;
    }

    public String getAppPackage() {
        return appPackage;
    }


    private String getUrlFromCloud(  ){

        String[] UPLOAD_CURL = new String[]{ "curl","-X","PUT","http://10.250.82.26/v3/project/5d022b953d25030592a3bdf2/build?accesskey=BHCTajO17_FeqD49fTRUQIWND4","-H","Content-Type:multipart/form-data","-F","build=@"+app};
        String uploadId = requestor.read(UPLOAD_CURL ).substring( 8 , 32 );
        String buildUrl = FrameworkConstants.DOWNLOAD_URL+uploadId+FrameworkConstants.FILE_TYPE+FrameworkConstants.ACCESS_KEY+FrameworkConstants.ACCESS_KEY_VALUE;
        return buildUrl;

    }



}




