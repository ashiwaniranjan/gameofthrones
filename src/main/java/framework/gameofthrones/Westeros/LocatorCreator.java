package framework.gameofthrones.Westeros;

import org.openqa.selenium.By;

import java.lang.reflect.Field;

import static framework.gameofthrones.Westeros.MobileSelector.*;


public abstract class LocatorCreator {

    private final String SPLITTER = "@#@";
    private final UiElementLoader loader = UiElementLoader.getInstance( UiLauncher.resourceName );
    private Field field;
    Locator locator;


    LocatorCreator(Field field) {
        this.field = field;
        this.locator = this.getByLocator( getData() );
    }



    public String getData() {

      return loader.getUIElementDetails(field
              .getDeclaringClass().getSimpleName()
              + "."
              + field.getName());
    }

    private Locator getByLocator(String locator) {
        if ( null == locator)
            throw new NullPointerException("Valid locator is required ");
        switch (locator.split(SPLITTER)[0]) {
            case "xpath":
                this.locator = new ByLocator(By.xpath(locator.split(SPLITTER)[1]));
                break;
            case "id":
                this.locator = new ByLocator(By.id(locator.split(SPLITTER)[1]));
                break;
            case "className":
                this.locator = new ByLocator(By.className(locator.split(SPLITTER)[1]));
                break;
            case "name":
                this.locator = new ByLocator(By.name(locator.split(SPLITTER)[1]));
                break;
            case "linkText":
                this.locator = new ByLocator(By.linkText(locator.split(SPLITTER)[1]));
                break;
            case "cssSelector":
                this.locator = new ByLocator(By.cssSelector(locator.split(SPLITTER)[1]));
                break;
            case "tagName":
                this.locator = new ByLocator(By.tagName(locator.split(SPLITTER)[1]));
                break;
            case "accessibilityId":
                this.locator = new AccessibilityLocator(locator.split(SPLITTER)[1]);
                break;
            case "androidUiAutomator":
                this.locator = new AutomationLocator(locator.split(SPLITTER)[1], ANDROID_UI_AUTOMATOR.toString());
                break;
            case "iOsClassChain":
                System.out.println( locator.split(SPLITTER)[1]  );
                this.locator = new AutomationLocator(locator.split(SPLITTER)[1],IOS_CLASS_CHAIN.toString());
                break;
            case "iOSPredicate":
                System.out.println(  locator.split(SPLITTER)[1] );
                this.locator = new AutomationLocator(locator.split(SPLITTER)[1],IOS_PREDICATE_STRING.toString());
                break;

            default:

        }
        return this.locator;
    }

}
