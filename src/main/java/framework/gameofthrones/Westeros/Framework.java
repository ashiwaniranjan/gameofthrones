package framework.gameofthrones.Westeros;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME )
@Target({ElementType.FIELD, ElementType.METHOD , ElementType.CONSTRUCTOR } )
public @interface Framework {

    long findTimeOut() default 0;

    long pollingTime() default 0;

    String appiumRuner() default "http://127.0.0.1:4723/wd/hub";

}