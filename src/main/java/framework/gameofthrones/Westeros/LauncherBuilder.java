package framework.gameofthrones.Westeros;

import com.google.api.client.util.Preconditions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;

public class LauncherBuilder {

    String deviceID;
    String deviceName;
    String systemPort;
    String appActivity;
    String appPackage;
    static String app;
    String chromeDriverPort;
    String wdaLocalPort;
    String webkitDebugProxyPort;
    DesiredCapabilities capabilities;
    String platformVersion;
    private String platformName;
    private String port;

    public LauncherBuilder(String appActivity, String appPackage ) {

        this.appActivity = appActivity;
        this.appPackage = appPackage;
    }

    public LauncherBuilder withDeviceID(String deviceID){
        Preconditions.checkNotNull(deviceID);
        this.deviceID = deviceID;
        return this;
    }

    public LauncherBuilder withDeviceName(String deviceName){
        Preconditions.checkNotNull(deviceName);
        this.deviceName = deviceName;
        return this;
    }

    public LauncherBuilder withSystemPort( String systemPort){
        Preconditions.checkNotNull(systemPort);
        this.systemPort = systemPort;
        return this;
    }

    public LauncherBuilder withChromeDriverPort( String chromeDriverPort ){
        Preconditions.checkNotNull(chromeDriverPort);
        this.chromeDriverPort = chromeDriverPort;
        return this;
    }

    public LauncherBuilder withWdaLocalPort( String wdaLocalPort ){
        Preconditions.checkNotNull(wdaLocalPort);
        this.wdaLocalPort = wdaLocalPort;
        return this;
    }

    public LauncherBuilder withWebKitDriverProxy( String webkitDebugProxyPort ){

        Preconditions.checkNotNull(webkitDebugProxyPort);
        this.webkitDebugProxyPort = webkitDebugProxyPort;
        return this;
    }

    public LauncherBuilder withCapabilities( DesiredCapabilities capabilities ){

        Preconditions.checkNotNull(capabilities);
        this.capabilities = capabilities;
        return this;

    }

    public LauncherBuilder withPlatformVersion( String platformVersion ){
        Preconditions.checkNotNull(platformVersion);
        this.platformVersion = platformVersion;
        return this;
    }

    public LauncherBuilder withPlatform( String platformName ){
        Preconditions.checkNotNull( platformName );
        this.platformName = platformName;
        return this;
    }

    public LauncherBuilder withPort( String port ){

        this.port = port;
        return  this;
    }

    public UiLauncher build(){
        UiLauncher launcher = new UiLauncher( this.appActivity , this.appPackage );
        launcher.deviceName = this.deviceName;
        launcher.webkitDebugProxyPort = this.webkitDebugProxyPort ;
        launcher.systemPort = this.systemPort;
        launcher.wdaLocalPort = this.wdaLocalPort;
        launcher.chromeDriverPort = this.chromeDriverPort ;
        launcher.deviceID = this.deviceID ;
        launcher.platformVersion = this.platformVersion;
        launcher.platformVersion = platformVersion;
        launcher.platformName = platformName;
        launcher.port = port;
        launcher.capabilities = this.capabilities != null ? this.capabilities : launcher.buildConfig( UiLauncher.platformName );
        try {
            launcher.setUpDriver( launcher.capabilities );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return launcher;

    }
}
