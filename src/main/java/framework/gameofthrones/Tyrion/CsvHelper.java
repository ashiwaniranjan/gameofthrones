package framework.gameofthrones.Tyrion;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CsvHelper {
    public static ArrayList<String> csvToList(String data) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (data != null) {
            String[] splitData = data.split("\\s*,\\s*");
            for (int i = 0; i < splitData.length; i++) {
                System.out.println(splitData[i]);
                if (!(splitData[i] == null) && !(splitData[i].length() == 0))) {
                    arrayList.add(splitData[i].trim());
                }
            }
        }
        return arrayList;
    }
    public static List<Object[]> ht() {
        BufferedReader reader;
        List<Object[]> lists=new ArrayList<>();
        String Line="";
        try {
            reader = new BufferedReader(new FileReader("/home/mmt8358/Downloads/data.csv"));
            while ((Line = reader.readLine()) != null) {
                lists.add(csvToList(Line).stream().toArray(String[]::new));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lists;
    }
    @DataProvider(name="a1")
    public static Iterator<Object[]> data() {
        return ht().iterator();
    }
     @DataProvider(name="parallelhandel",parallel = true)
    public static Iterator<Object[]> datas() {
        return ht().iterator();
    }
    @Test(dataProvider = "a1")
    public void t(String a1,String a2,String a3,String a4,String a5,String a6){
        System.out.println(a1+a2+a3+a4);
    }
}