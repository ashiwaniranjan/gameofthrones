package framework.gameofthrones.Tyrion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * Generic Template
 * @author abhijit.p
 */
public class NamedGenericTemplate {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private static Logger log = LoggerFactory.getLogger(NamedGenericTemplate.class);

    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    public List<Map<String, Object>> query(String sql,MapSqlParameterSource parameters){
        List<Map<String, Object>> list= this.namedParameterJdbcTemplate.queryForList(sql, parameters);
        return list;
    }









}