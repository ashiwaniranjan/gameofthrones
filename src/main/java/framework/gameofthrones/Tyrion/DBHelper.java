package framework.gameofthrones.Tyrion;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Connstring;
import framework.gameofthrones.Daenerys.Initializer;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class DBHelper {

    //Initialize init = new Initialize();
    Initialize init =Initializer.getInitializer();

    public SqlTemplate getMySqlTemplate(String name){
        com.zaxxer.hikari.HikariDataSource dataSource = new com.zaxxer.hikari.HikariDataSource();
        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getDatabase().getConnectionstrings().getDBDetails(name);
        Properties properties = new Properties();
        if(connstring.getProtocol().equalsIgnoreCase("mysql")){
            dataSource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
            dataSource.setIdleTimeout(3000);
            dataSource.setMaximumPoolSize(1);
            dataSource.setConnectionTimeout(10000);
            dataSource.setValidationTimeout(5000);
            dataSource.setInitializationFailFast(true);
            dataSource.setMaxLifetime(20000);
            dataSource.setAutoCommit(true);
            properties.setProperty("port", String.valueOf(connstring.getPort()));
        }else{
            dataSource.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
            //properties.setProperty("validationQuery","SELECT 1 ");
        }
        dataSource.setUsername(connstring.getUsername());
        dataSource.setPassword(connstring.getPassword());
        properties.setProperty("serverName", connstring.getServer());
        properties.setProperty("databaseName", connstring.getDbid());
        dataSource.setDataSourceProperties(properties);
        GenericTemplate dbTemplate = new GenericTemplate();
        dbTemplate.setDataSource(dataSource);
        return dbTemplate;
    }

    /**
     * DB polling through Key and Value
     * @param dbname
     * @param Query
     * @param toSearchKey
     * @param toSearchValue
     * @param pollIntervalInSeconds
     * @param maxPollTimeInSecs
     * @return boolean
     */
    public static boolean pollDB(String dbname, String Query, String toSearchKey, String toSearchValue, int pollIntervalInSeconds, int maxPollTimeInSecs) {
        boolean exitFlag = false;
        long startTime = System.currentTimeMillis();
        System.out.println("current time in search and select "+ startTime);
        long maxDurationInMilliseconds = maxPollTimeInSecs * 1000;
        long endTime = System.currentTimeMillis() + maxDurationInMilliseconds;
        SqlTemplate template= SystemConfigProvider.getTemplate(dbname);
        List<Map<String, Object>> dbMapList = null;
        do{
            dbMapList = template.queryForList(Query);
            System.out.println("RETRYING ..## "+ dbMapList);
            if(dbMapList.size()>0 && dbMapList.get(0).get(toSearchKey).toString().equals(toSearchValue)) {
                return true;
            }
            else {
                try {
                    Thread.sleep(pollIntervalInSeconds *1000);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("printing conditions"+(exitFlag==false) +"#########"+ (System.currentTimeMillis() > endTime));

        } while(exitFlag==false && System.currentTimeMillis() < endTime);
        return exitFlag;
    }
    public NamedGenericTemplate getMySqlTemplateNamed(String name){
        com.zaxxer.hikari.HikariDataSource dataSource = new com.zaxxer.hikari.HikariDataSource();
        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getDatabase().getConnectionstrings().getDBDetails(name);
        Properties properties = new Properties();
        if(connstring.getProtocol().equalsIgnoreCase("mysql")){
            dataSource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
            dataSource.setIdleTimeout(200);
            dataSource.setMaximumPoolSize(1);
            dataSource.setConnectionTimeout(10000);
            dataSource.setValidationTimeout(5000);
            dataSource.setInitializationFailFast(true);
            dataSource.setMaxLifetime(200);
            dataSource.setAutoCommit(true);
            properties.setProperty("port", String.valueOf(connstring.getPort()));
        }else{
            dataSource.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
            //properties.setProperty("validationQuery","SELECT 1 ");
        }
        dataSource.setUsername(connstring.getUsername());
        dataSource.setPassword(connstring.getPassword());
        properties.setProperty("serverName", connstring.getServer());
        properties.setProperty("databaseName", connstring.getDbid());
        dataSource.setDataSourceProperties(properties);
        NamedGenericTemplate dbTemplate = new NamedGenericTemplate();
        dbTemplate.setDataSource(dataSource);
        return dbTemplate;
    }

}