package framework.gameofthrones.Tyrion;

import com.datastax.driver.core.*;


public class ScyllaDBHelper {

    static String CONTACT_POINTS_1 = "ec2-54-255-168-102.ap-southeast-1.compute.amazonaws.com";//{"54.255.168.102"};
    static String CONTACT_POINTS_2 = "ec2-54-255-185-28.ap-southeast-1.compute.amazonaws.com";
    String CASSADRA_URL = "127.0.0.1";//"jdbc:cassandra://ec2-54-255-168-102.ap-southeast-1.compute.amazonaws.com:9042/rule";//"54.255.168.102";
    static int PORT = 9042;

    public static void main(String[] args) {

        ScyllaDBHelper client = new ScyllaDBHelper();

        try {

            client.connect("");
//            client.querySchema();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            client.close();
        }
    }

    private Cluster cluster;

    private Session session;

    public void connect(String contactPoints) {

        cluster = Cluster.builder().addContactPoints("127.0.0.1").withPort(9042).build();

        cluster.getConfiguration().getSocketOptions().setReadTimeoutMillis(100000);
        System.out.println("***************" + cluster.getClusterName());
        session = cluster.connect();
    }


    public void querySchema() {

        ResultSet results =
                session.execute(
                        "SELECT * FROM rule.entities;");

        System.out.printf("%n", "reference_id");
        System.out.println(
                "-------------------------------+-----------------------+--------------------");

        for (Row row : results) {

            System.out.printf(
                    "%n",
                    row.getString("reference_id"));
        }
    }

    /** Closes the session and the cluster. */
    public void close() {
        session.close();
        cluster.close();
    }

}