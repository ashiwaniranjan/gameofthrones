package framework.gameofthrones.Tyrion;

import com.orbitz.consul.Consul;
import lombok.*;

import java.util.Optional;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConsulHelper {

    Consul consulClient;

    public ConsulHelper(String url)
    {
        getConsulClient(url);
    }

    private Consul getConsulClient() {
        if(consulClient!=null)
        {
            return consulClient;
        }else
        {
            consulClient = Consul.builder().withUrl("http://consul.u4.swiggyops.de").build();
        }
        return consulClient;
    }

    private Consul getConsulClient(String url) {
        if(consulClient!=null)
        {
            return consulClient;
        }else
        {
            consulClient = Consul.builder().withUrl(url).build();
        }
        return consulClient;
    }

    public Optional<String> getValue(String key)
    {
        return consulClient.keyValueClient().getValueAsString(key);
    }

    public boolean updateValue(String key, String val)
    {
        return consulClient.keyValueClient().putValue(key,val);
    }

}
