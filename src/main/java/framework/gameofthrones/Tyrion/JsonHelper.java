package framework.gameofthrones.Tyrion;

import org.apache.poi.ss.formula.functions.T;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.PropertyNamingStrategy;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class JsonHelper {

    /**
     * Convert JavaObject to JSON
     *
     * @param className
     * @return String
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     */
    public String getObjectToJSON(Object className) throws IOException {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        json = mapper.writeValueAsString(className);
        return json;

    }
    public String getObjectToJSONWithNaming(Object className) throws IOException {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        json = mapper.writeValueAsString(className);
        return json;

    }

    public String getObjectToJSONallowNullValues(Object className) throws IOException {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        json = mapper.writeValueAsString(className);
        return json;
    }

    public Map<String, Object> getJSONToMap(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<String, Object>();
        mapper.configure(SerializationConfig.Feature.AUTO_DETECT_FIELDS, true);
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
        return map;
    }


    public Object getObjectFromJson(String json, String className)
    {
        ObjectMapper mapper = new ObjectMapper();
        try{
            Class ResponseClass = Class.forName(className);
            Object responseObject = ResponseClass.newInstance();
            responseObject = mapper.readValue(json,responseObject.getClass());
            return responseObject;

        }
        catch(ClassNotFoundException cnfe)
        {
            System.out.println("JSON to POJO Mapping Error:  Unable to find Class "+className);
            System.out.println("Error :"+cnfe.getMessage());
            return null;
        }

        catch(InstantiationException ie)
        {
            System.out.println("JSON to POJO Mapping Error:  Unable to Intantiate Class "+className);
            System.out.println("Error :"+ie.getMessage());
            return null;
        }

        catch(Exception e)
        {
            System.out.println("JSON to POJO Mapping Error:   "+e.getMessage());
            return null;
        }

    }



}
