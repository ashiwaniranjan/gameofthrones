package framework.gameofthrones.JonSnow;

import framework.gameofthrones.Daenerys.environment;

/**
 * Created by ashish.bajpai on 08/05/17.
 */
public class WebServiceDetails {
    public String Webservice;
    public String Core;
    public String Baseurl;
    public String AuthRequired;
    public String UserID;
    public String SecurityToken;
    public String Password;

    public WebServiceDetails(String ServiceName, environment Env){
        GetWebServiceDetails(ServiceName, Env);
    }

    public WebServiceDetails(String webservice, String core, String baseurl,
                             String authrequired, String userid, String securityToken,
                             String password){
        GetWebServiceDetails(webservice, core, baseurl, authrequired, userid, securityToken, password);
    }
    private void GetWebServiceDetails(String servicename, environment Env){
        Webservice = Env.getServices().GetServiceDetails(servicename).getServicename();
//        System.out.println("Webservice = " + Webservice);
        Core = Env.getServices().GetServiceDetails(servicename).getCore();
        Baseurl = Env.getServices().GetServiceDetails(servicename).getBaseurl();
        AuthRequired = (Env.getServices().GetServiceDetails(servicename).getAuthenticationrequired().trim().toLowerCase());
        UserID  = Env.getServices().GetServiceDetails(servicename).getUserid();
        SecurityToken  = Env.getServices().GetServiceDetails(servicename).getSecuritytoken();
        Password  = Env.getServices().GetServiceDetails(servicename).getPassword();
    }

    private void GetWebServiceDetails(String webservice, String core, String baseurl,
                                      String authrequired, String userid, String securityToken,
                                      String password){
        Webservice = webservice;
        Core = core;
        Baseurl = baseurl;
        AuthRequired = authrequired;
        UserID  = userid;
        SecurityToken  = securityToken;
        Password  = password;
    }
}
