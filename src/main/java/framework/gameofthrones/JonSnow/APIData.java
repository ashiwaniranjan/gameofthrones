package framework.gameofthrones.JonSnow;

import framework.gameofthrones.Daenerys.api;
import framework.gameofthrones.Daenerys.environment;


/**
 * Created by ashish.bajpai on 06/05/17.
 */
@SuppressWarnings("Since15")
public class APIData {
    public String Webservice;
    public String Core;
    public String Baseurl;
    public String AuthRequired;
    public String UserID;
    public String SecurityToken;
    public String Password;
    public String APIName;
    public String APIPath;
    public String RequestMethod;
    public String PayloadRequired;
    public String QueryParamsRequired;
    public String PayloadBody;
    public String PayloadType;
    public String ServiceName;
    public String Accept;

    APIDetails apidetails;
    public String CompleteURL;

    WebServiceDetails webservicedata;

    public APIData(String ServiceName, String apiname, environment Env){

    	
        //webservicedata  = new WebServiceDetails(ServiceName, Env);
        MapWebservicedetails(ServiceName, apiname, Env);
        MapAPIDetails(ServiceName, apiname,Env);
        //GetAPIDetails(ServiceName, apiname, Env);
//
    }

    private void MapAPIDetails(APIDetails apidata){
        //APIDetails apiinfo = new APIDetails(apidata.APIName, apidata.APIPath, apidata.RequestMethod,  apidata.PayloadRequired,apidata.QueryParamsRequired, apidata.PayloadType, apidata.ServiceName);
        APIDetails apiinfo = new APIDetails(apidata.APIName, apidata.APIPath, apidata.RequestMethod,  apidata.PayloadRequired,apidata.QueryParamsRequired, apidata.PayloadType, apidata.ServiceName, apidata.Accept);
        apidetails = apiinfo;
    }


    private void MapAPIDetails(String serviceName, String apiname, environment Env){

        api apidetail = Env.getServices().getApis().GetAPIDetails(serviceName, apiname);
       // System.out.println("Env = " + Env.getServices().getApis().GetAPIDetails(apiname).getApiname());
        APIName = apidetail.getApiname();
        APIPath = apidetail.getPath();
        RequestMethod = apidetail.getRequestmethod();
        QueryParamsRequired = apidetail.getQueryparamsrequired();
        PayloadRequired = apidetail.getPayloadrequired();
        PayloadType = apidetail.getPayloadtype();
        ServiceName = apidetail.getServicename();
        //APIDetails apiinfo = new APIDetails(APIName, APIPath, RequestMethod,  PayloadRequired, QueryParamsRequired, PayloadType, ServiceName);
        Accept = apidetail.getAccept();
        APIDetails apiinfo = new APIDetails(APIName, APIPath, RequestMethod,  PayloadRequired, QueryParamsRequired, PayloadType, ServiceName, Accept);
        apidetails = apiinfo;
    }


    private void MapWebservicedetails(WebServiceDetails wsDetails){
        webservicedata = wsDetails;

    }

    private void MapWebservicedetails(String ServiceName, String apiname, environment Env){
        webservicedata  = new WebServiceDetails(ServiceName, Env);
        Webservice = webservicedata.Webservice;
        //System.out.println("Webservice = " + Webservice);
        Core = webservicedata.Core;
        Baseurl = webservicedata.Baseurl;
        AuthRequired = webservicedata.AuthRequired;
        UserID  = webservicedata.UserID;
        SecurityToken  = webservicedata.SecurityToken;
        Password  = webservicedata.Password;
    }

    public APIData(WebServiceDetails webServiceDetails, APIDetails apidtails){
        MapWebservicedetails(webServiceDetails);
        MapAPIDetails(apidtails);
    }

    /*public APIData(WebServiceDetails WebserviceInfo, String core, String baseurl, String authRequired, ){
        webservicedata = WebserviceInfo;
        APIName = apidetails.getApiname();
        APIPath = apidetails.getPath();
        RequestMethod = apidetails.getRequestmethod();
        QueryParamsRequired = apidetails.getQueryparamsrequired();
        PayloadRequired = apidetails.getPayloadrequired();

        CompleteURL = createpath();

    }*/

    public void GetAPIDetails(String ServiceName, String apiname, environment Env){
        /*//Env.getServices().GetServiceDetails("catalog");
        endpoint ep = Env.getServices().GetServiceDetails(servicename);
        Webservice = Env.getServices().GetServiceDetails(servicename).getServicename();
        System.out.println("Webservice = " + Webservice);
        Core = Env.getServices().GetServiceDetails(servicename).getCore();
        Baseurl = Env.getServices().GetServiceDetails(servicename).getBaseurl();
        AuthRequired = (Env.getServices().GetServiceDetails(servicename).getAuthenticationrequired().trim().toLowerCase());
        UserID  = Env.getServices().GetServiceDetails(servicename).getUserid();
        SecurityToken  = Env.getServices().GetServiceDetails(servicename).getSecuritytoken();
        Password  = Env.getServices().GetServiceDetails(servicename).getPassword();
        //APIPath = Env.getServices().GetServiceDetails(servicename).getAPIDetails(ep, "location").getPath();
        //System.out.println("APIPath = " + APIPath);

        //String data = Env.getServices().getApis().GetAPIDetails("pgresponse").getPath();
        List<api> lst = Env.getServices().getApis().getApi();
        Iterator<api> itr = lst.iterator();
        while (itr.hasNext()){
            String nm = itr.next().getApiname();
            //System.out.println("nm = " + nm);
        }

        /*for (api:
             Env.getServices().GetServiceDetails(APIName).GetAPIs("apiname");) {

        }*/
        //System.out.println("APIName = " + Env.getServices().GetServiceDetails("catalog").getBaseurl());

        Webservice = webservicedata.Webservice;
       // System.out.println("Webservice = " + Webservice);
        Core = webservicedata.Core;
        Baseurl = webservicedata.Baseurl;
        AuthRequired = webservicedata.AuthRequired;
        UserID  = webservicedata.UserID;
        SecurityToken  = webservicedata.SecurityToken;
        Password  = webservicedata.Password;

        api apidetail = Env.getServices().getApis().GetAPIDetails(ServiceName, apiname);

        APIName = apidetail.getApiname();
        APIPath = apidetail.getPath();
        RequestMethod = apidetail.getRequestmethod();
        QueryParamsRequired = apidetail.getQueryparamsrequired();
        PayloadRequired = apidetail.getPayloadrequired();
        PayloadType = apidetail.getPayloadtype();
        ServiceName = apidetail.getServicename();
        CompleteURL = createpath();


    }



    @SuppressWarnings("Since15")
    private String createpath() {
        /*StringJoiner joiner = new StringJoiner(File.pathSeparator); //Separator
        joiner.add(Baseurl).add(APIPath);
        String joinedString = joiner.toString();
        return joinedString;*/
        //Path filepath = Paths.get(Baseurl, APIPath);
        String filepath = Baseurl+"/"+APIPath;
        return String.valueOf(filepath);
    }
}
