package framework.gameofthrones.JonSnow;
import framework.gameofthrones.Daenerys.Queue;
import framework.gameofthrones.Daenerys.environment;


public class QueueData {

    public String Hostname;
    public String HostUrl;
    public int Port;
    public String Name;
    public String Exchangekey;
    public String Routingkey;
    public String Vhost;
    public String username;
    public String password;



    public QueueData(String HostName,String QueueName,environment Env){
        GetQueueDetails(HostName,QueueName, Env);
    }

    private void GetQueueDetails(String HostName,String QueueName,environment Env) {
        Hostname = Env.getQueues().GetQueueConnectionDetails(HostName).getHostname();
        HostUrl = Env.getQueues().GetQueueConnectionDetails(HostName).getHosturl();
        Port = Env.getQueues().GetQueueConnectionDetails(HostName).getPort();
        username = Env.getQueues().GetQueueConnectionDetails(HostName).getUsername();
        password = Env.getQueues().GetQueueConnectionDetails(HostName).getPassword();
        Queue queueDetails= Env.getQueues().GetQueueConnectionDetails(HostName).GetQueueDetails(QueueName);
        Name=queueDetails.getName();
        Exchangekey=queueDetails.getExchangekey();
        Routingkey=queueDetails.getRoutingkey();
        Vhost=queueDetails.getVhost();
    }

}
