package framework.gameofthrones.Cersei;

/**
 * Created by ashish.bajpai on 13/11/17.
 */
public class MailReportSummary {
    public String Environment;
    public String TotalExecutedTests;
    public String TotalFailedTests;
    public String TotalPassedTests;
    public String TotalSkippedTests;
    public String TotaldataproviderSkip;
    public String OverallResult;
    public String ExecutionTime;

    public MailReportSummary(String environment, String totalExecutedTests, String totalFailedTests,  String totalPassedTests, String totalSkippedTests, String overallResult, String executionTime){
        Environment = environment;
        TotalExecutedTests = totalExecutedTests;
        TotalFailedTests = totalFailedTests;
        TotalPassedTests = totalPassedTests;
        TotalSkippedTests = totalSkippedTests;
        ExecutionTime = executionTime;
        OverallResult = "FAIL";

        if (Integer.valueOf(totalSkippedTests)>0){
            OverallResult = "Incomplete Run";
        }

        if (Integer.valueOf(totalFailedTests)==0 || Integer.valueOf(totalSkippedTests)==0 ){
            OverallResult = "PASS";
        }
    }

    public MailReportSummary(String environment, String totalExecutedTests, String totalFailedTests,  String totalPassedTests, String totalSkippedTests, String overallResult, String executionTime,String dataproviderSkip){
        Environment = environment;
        TotalExecutedTests = totalExecutedTests;
        TotalFailedTests = totalFailedTests;
        TotalPassedTests = totalPassedTests;
        TotalSkippedTests = totalSkippedTests;
        ExecutionTime = executionTime;
        TotaldataproviderSkip = dataproviderSkip;
        OverallResult = "FAIL";

        if (Integer.valueOf(totalSkippedTests)>0){
            OverallResult = "Incomplete Run";
        }

        if (Integer.valueOf(totalFailedTests)==0 || Integer.valueOf(totalSkippedTests)==0 ){
            OverallResult = "PASS";
        }
    }
}
