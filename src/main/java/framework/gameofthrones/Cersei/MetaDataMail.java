package framework.gameofthrones.Cersei;

/**
 * Created by ashish.bajpai on 16/11/17.
 */
public class MetaDataMail {

    public String SubjectLine;
    public String ToEmails;
    public String BCCEmails;
    public String CCEmails;


    public MetaDataMail(String toemails, String bccremails, String ccEmails, String subject){
        ToEmails = toemails;
        BCCEmails = bccremails;
        CCEmails = ccEmails;
        SubjectLine = subject;

    }

}
