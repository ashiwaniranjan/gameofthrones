package framework.gameofthrones.Cersei;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.StaticData;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.hudor.HodorPage;
import framework.gameofthrones.Daenerys.Initializer;
import org.testng.util.Strings;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashish.bajpai on 03/11/17.
 */
public class TestExecutionReport {

    /*public String TestName;
    public String Result;
    public String FailureReason;
    public String Parameters;
    public String TotalExecuted;
    public String TotalPassed;
    public String TotalFailed;
    public String OtherStatus;
    public String PassPercentage;
    public String StartTime;
    public String EndTime;
    public String ExecutionTime;
    public int executiontrycount;
    */
    private String ExecutionReportAppender;
    private static String FinalReport;
    //private PropertiesHandler props;
    private String reportdelta = "";
    private String fullreporttemplate = GetEmailRawBody(MailTemplate.TESTRUNREPORT);
    private String emaillist = "";
    private MailData maildata;
    //public String Environment="PRODUCTION";
    private String Subject;
    //private Initialize initialize;
    public TestExecutionReport(List<TestExecutionReportRow> data,String EmailList) {

        //props = init.Properties;
        //initialize = init;
        int sno = 1;
        emaillist = EmailList;
        //System.out.println("reportdelta = " + reportdelta);

        System.out.println("data = " + data.size());

        for (TestExecutionReportRow row:sortedresultlist(data)) {
            String newrow = getrowhtml(sno);

            newrow = newrow.replace("${sno}",String.valueOf(sno));
            newrow = newrow.replace("${testname}", handlenullsorEmptyString(row.TestName));
            newrow = newrow.replace("${result}", row.Result);
            newrow = newrow.replace("${params}", row.Parameters);
            newrow = newrow.replace("${reason}", handlenullsorEmptyString(row.FailureReason));
            if (row.Result == "FAIL"){
                newrow = newrow.replace("${color}", "FF5555");
            }
            else
            {
                newrow = newrow.replace("${color}", "55FF55");
            }
            //reportdelta.concat(newrow);
            reportdelta = reportdelta+newrow;
            //System.out.println("newrow = " + reportdelta);
            sno++;


        }
        //System.out.println("reportdelta = " + reportdelta);
        String finalmailbody = fullreporttemplate.replace("${rows}", reportdelta);
        FinalReport = finalmailbody;
        //sendhtmlreport();


    }

    public TestExecutionReport(MailData data) {

        //props = init.Properties;
        //initialize = init;
        maildata = data;
        Initialize gameofthrones = Initializer.getInitializer();
//        Initialize gameofthrones = new Initialize();
        String Environment=gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getName();
        //Subject = maildata.MailMeta.SubjectLine.replace("${Env}",Environment);
        //maildata.MailMeta.SubjectLine = Subject;
        int sno = 1;
        //emaillist = data.mailMeta.SubjectLine;
        //System.out.println("reportdelta = " + reportdelta);
        System.out.println("data = " + data.reportRows.size());
        for (TestExecutionReportRow row:sortedresultlist(data.reportRows)) {
            String newrow = getrowhtml(sno);

            newrow = newrow.replace("${sno}",String.valueOf(sno));
            newrow = newrow.replace("${testname}", handlenullsorEmptyString(row.TestName));
            newrow = newrow.replace("${result}", row.Result);
            if (row.Result == "FAIL")
                newrow = newrow.replace("${params}", row.Parameters);
            else
                newrow = newrow.replace("${params}", "");
            newrow = newrow.replace("${reason}", filterFailureMessage(handlenullsorEmptyString(row.FailureReason)));
            if (row.Result == "FAIL"){
                newrow = newrow.replace("${color}", "FF5555");
            }
            else
            {
                newrow = newrow.replace("${color}", "228B22");
            }
            //reportdelta.concat(newrow);
            reportdelta = reportdelta+newrow;
            //System.out.println("newrow = " + reportdelta);
            sno++;

        }
        //System.out.println("reportdelta = " + reportdelta);
        String finalmailbody = fullreporttemplate.replace("${rows}", reportdelta);
        //finalmailbody = fullreporttemplate.replace("${rows}", reportdelta);
        FinalReport = finalmailbody;
        finalisesummarysection();
        //sendhtmlreport(data);
    }
    public TestExecutionReport(MailData data,String serviceName) {

        //props = init.Properties;
        //initialize = init;
        String buildNumber = getBuildNumber(serviceName);
        maildata = data;
        Initialize gameofthrones = new Initialize();
        String Environment=gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getName();
        //Subject = maildata.MailMeta.SubjectLine.replace("${Env}",Environment);
        //maildata.MailMeta.SubjectLine = Subject;
        int sno = 1;
        //emaillist = data.mailMeta.SubjectLine;
        //System.out.println("reportdelta = " + reportdelta);
        System.out.println("data = " + data.reportRows.size());
        for (TestExecutionReportRow row:sortedresultlist(data.reportRows)) {
            String newrow = getrowhtml(sno);

            newrow = newrow.replace("${sno}",String.valueOf(sno));
            newrow = newrow.replace("${testname}", handlenullsorEmptyString(row.TestName));
            newrow = newrow.replace("${result}", row.Result);
            if (row.Result == "FAIL")
                newrow = newrow.replace("${params}", row.Parameters);
            else
                newrow = newrow.replace("${params}", "");
            newrow = newrow.replace("${reason}", filterFailureMessage(handlenullsorEmptyString(row.FailureReason)));
            if (row.Result == "FAIL"){
                newrow = newrow.replace("${color}", "FF5555");
            }
            else
            {
                newrow = newrow.replace("${color}", "228B22");
            }
            //reportdelta.concat(newrow);
            reportdelta = reportdelta+newrow;
            //System.out.println("newrow = " + reportdelta);
            sno++;

        }
        //System.out.println("reportdelta = " + reportdelta);
        String finalmailbody = fullreporttemplate.replace("${rows}", reportdelta);
        //finalmailbody = fullreporttemplate.replace("${rows}", reportdelta);
        FinalReport = finalmailbody;
        finalisesummarysection(buildNumber);
        //sendhtmlreport(data);
    }

    String getEnvValue(String envName)
    {
        String value="http://gameofthronesjenkins.swiggyops.de/";
        Map<String, String> env = System.getenv();
        for (String name : env.keySet()) {
            System.out.format("env name: %s=%s%n", envName, env.get(name));
            if(envName.equals(name)) {
                value = env.get(name);
                return value;
            }
        }
        return value;
    }

    private void finalisesummarysection(String buildNumber){


        StringBuffer jenkinJob = new StringBuffer();
        jenkinJob.append(getEnvValue("JOB_URL"));
        jenkinJob.append(getEnvValue("BUILD_ID"));

        String jenkinUrl = jenkinJob.toString();
        System.out.println("jenkinurl: "+ jenkinUrl);

        System.out.println("run id:"+SystemConfigProvider.getRunId());
        String gotUlr = "http://swiggyqaone.swiggyops.de/api/shadowtower/shadowtower/gettests/"+SystemConfigProvider.getRunId()+"/runs";
        System.out.println("got url:"+gotUlr);

        Initialize gameofthrones = Initializer.getInitializer();
//        Initialize gameofthrones = new Initialize();
        String Environment=gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getName();
        FinalReport = FinalReport.replace("${environment}", Environment);
        int totalTest = Integer.parseInt(maildata.mailReportSummary.TotalFailedTests) + Integer.parseInt(maildata.mailReportSummary.TotalPassedTests);
        FinalReport = FinalReport.replace("${totaltests}",""+totalTest);
        int totalFailed = (Integer.parseInt(maildata.mailReportSummary.TotalFailedTests))+ (Integer.parseInt(maildata.mailReportSummary.TotaldataproviderSkip));
        FinalReport = FinalReport.replace("${failed}",  ""+totalFailed);
        FinalReport = FinalReport.replace("${passed}", maildata.mailReportSummary.TotalPassedTests);
        //FinalReport = FinalReport.replace("${skipped}", maildata.mailReportSummary.TotalSkippedTests);
        FinalReport = FinalReport.replace("${executiontime}", maildata.mailReportSummary.ExecutionTime);
        FinalReport = FinalReport.replace("${jenkinurl}", jenkinUrl);
        FinalReport = FinalReport.replace("${goturl}", gotUlr);
        FinalReport = FinalReport.replace("${time}", HodorPage.getDateInReadableFormatWithSecond());
        FinalReport = FinalReport.replace("${buildnumber}", buildNumber);


    }
    private void finalisesummarysection(){


        StringBuffer jenkinJob = new StringBuffer();
        jenkinJob.append(getEnvValue("JOB_URL"));
        jenkinJob.append(getEnvValue("BUILD_ID"));

        String jenkinUrl = jenkinJob.toString();
        System.out.println("jenkinurl: "+ jenkinUrl);

        System.out.println("run id:"+SystemConfigProvider.getRunId());
        String gotUlr = "http://swiggyqaone.swiggyops.de/api/shadowtower/gettests/"+SystemConfigProvider.getRunId()+"/runs";
        System.out.println("got url:"+gotUlr);

        Initialize gameofthrones = new Initialize();
        String Environment=gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getName();
        FinalReport = FinalReport.replace("${environment}", Environment);
        int totalTest = Integer.parseInt(maildata.mailReportSummary.TotalFailedTests) + Integer.parseInt(maildata.mailReportSummary.TotalPassedTests);
        FinalReport = FinalReport.replace("${totaltests}",""+totalTest);
        int totalFailed = (Integer.parseInt(maildata.mailReportSummary.TotalFailedTests))+ (Integer.parseInt(maildata.mailReportSummary.TotaldataproviderSkip));
        FinalReport = FinalReport.replace("${failed}",  ""+totalFailed);
        FinalReport = FinalReport.replace("${passed}", maildata.mailReportSummary.TotalPassedTests);
        //FinalReport = FinalReport.replace("${skipped}", maildata.mailReportSummary.TotalSkippedTests);
        FinalReport = FinalReport.replace("${executiontime}", maildata.mailReportSummary.ExecutionTime);
        FinalReport = FinalReport.replace("${jenkinurl}", jenkinUrl);
        FinalReport = FinalReport.replace("${goturl}", gotUlr);
        FinalReport = FinalReport.replace("${time}", HodorPage.getDateInReadableFormatWithSecond());

    }

    private String handlenullsorEmptyString(String inputstring){
        if(Strings.isNullOrEmpty(inputstring)) {
            inputstring = " ";
        }
        return inputstring;
    }

    private String filterFailureMessage(String inputstring) {
        String trueAssert = "expected [true] but found [false]";
        String failAssert = "expected [false] but found [true]";
        return inputstring.replace(trueAssert,"").replace(failAssert,"").trim();
    }


    private String getrowhtml(int i){
        String row="";
        if(i%2==0)
            row = "<tr><td>${sno}</td><td>${testname}</td><td><font color=\"#${color}\"><strong>${result}</strong></font></td><td>${reason}</td><td>${params}</td></tr>";
        else
            row = "<tr bgcolor=\"#eee\"><td>${sno}</td><td>${testname}</td><td><font color=\"#${color}\"><strong>${result}</strong></font></td><td>${reason}</td><td>${params}</td></tr>";
        return row;
    }

    /*private void sendhtmlreport(){
        MailHandler mailer = new MailHandler(FinalReport, emaillist);
    }*/


    public void sendhtmlreport(){
        //MailHandler mailer = new MailHandler(maildata,FinalReport);
        File f = new File("newreport.html");
        BufferedWriter bw=null;
        try {
            bw = new BufferedWriter(new FileWriter(f));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.write(FinalReport);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String GetEmailRawBody(MailTemplate template){
        String emailbody = "";
        try {
            StaticData data = new StaticData();
            ToolBox tools = new ToolBox();
            String templatepath = data.getTestRunReportPath();

            switch (template) {
                case TESTRUNREPORT:
                    emailbody = tools.readFileAsString(templatepath);
                    break;
                case TESTSUMMARY:
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return emailbody;

    }

    public List<TestExecutionReportRow> sortedresultlist(List<TestExecutionReportRow> data){

        List<TestExecutionReportRow> sorteddata = new ArrayList<TestExecutionReportRow>();

        for (TestExecutionReportRow row: data ) {
            if (row.Result.trim().equalsIgnoreCase("FAIL")){
                sorteddata.add(row);
            }
        }
        for (TestExecutionReportRow row: data ) {
            if (!row.Result.trim().equalsIgnoreCase("FAIL")){
                sorteddata.add(row);
            }
        }
        return sorteddata;
    }

    public String getBuildNumber(String serviceName)
    {
        if(serviceName.equalsIgnoreCase("NA"))
        {
            return serviceName;
        }
        else {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Content-Type", "application/json");
            Initialize init = new Initialize();
            GameOfThronesService gots = new GameOfThronesService("consulurl", "getconsulkey", init);
            String[] params_1 = {"daily-discovery-engine", "BUILD_NUMBER"};
            Processor p = new Processor(gots, hm, null, params_1);
            return p.ResponseValidator.GetBodyAsText();
        }
    }




}