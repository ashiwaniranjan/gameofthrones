package framework.gameofthrones.Cersei;

/**
 * Created by ashish.bajpai on 03/11/17.
 */
public class TestExecutionReportRow {
    public String TestName;
    public String Result;
    public String FailureReason;
    public String Parameters;

    public TestExecutionReportRow(String testname, String result,
                                  String parameters, String failurereason){
        TestName = testname;
        Result = result;
        FailureReason = failurereason;
        Parameters = parameters;

    }
}
