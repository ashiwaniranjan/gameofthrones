package framework.gameofthrones.Cersei;


import java.util.List;

public class MailData{

    public MailReportSummary mailReportSummary;
    public List<TestExecutionReportRow> reportRows;
    public MetaDataMail MailMeta;

    public MailData(MailReportSummary summary, List<TestExecutionReportRow> reportdetails){
        mailReportSummary = summary;
        reportRows = reportdetails;
    }

}