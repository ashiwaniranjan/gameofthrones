package framework.gameofthrones.Cersei;

/**
 * Created by ashish.bajpai on 29/10/17.
 */


import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.StaticData;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

public class MailHandler
{
    private static HtmlEmail mail;
    private StaticData staticdata = new StaticData();
    static Logger log = Logger.getLogger(MailHandler.class);
    Initialize myconfig = null;
    private static EmailAttachment attachment = new EmailAttachment();
    private String emaillist = "";




    public MailHandler(Initialize config, String finalreport){
        myconfig = config;
        staticdata = new StaticData(myconfig.Properties);
        mail = PrepareRawMailer("ashish.bajpai@swiggy.in","","[Test Execution Report] - ERP - OMS - Sanity - Failed", MailTemplate.TESTRUNREPORT, finalreport);
    }


    public MailHandler(Initialize config){
        myconfig = config;
        staticdata = new StaticData(myconfig.Properties);
        mail = PrepareRawMailer("ashish.bajpai@swiggy.in","","[Test Execution Report] - ERP - OMS - Sanity - Failed", MailTemplate.TESTRUNREPORT, "this is test report");
    }


    public MailHandler(String finalreport, String EmailList){
        //myconfig = config;
        emaillist = EmailList;
        staticdata = new StaticData();
        mail = PrepareRawMailer(emaillist,"","[Test Execution Report] - ERP - OMS - Sanity - Failed", MailTemplate.TESTRUNREPORT, finalreport);
    }

    public MailHandler(MailData data, String finalreport) {
        emaillist = data.MailMeta.ToEmails;
        staticdata = new StaticData();
        mail = PrepareRawMailer(data.MailMeta.ToEmails,"",data.MailMeta.SubjectLine, MailTemplate.TESTRUNREPORT, finalreport);
    }

    private HtmlEmail PrepareRawMailer(String reciepients, String bccreciepients, String Subject, MailTemplate Mailtype, String htmlmailbody){
        try{
            HtmlEmail mail = MailerConfig();
            System.out.println("reciepients = " + reciepients);
            mail.setFrom("swiggytest@swiggy.in", "Swiggy Test Engineering");
            String[] recipients = reciepients.split(",");
            for (int i = 0; i < recipients.length; i++)
            {
                mail.addTo(recipients[i]);
            }
            //mail.addTo(reciepients);
            URL url = new URL("https://www.google.com/a/swiggy.in/images/logo.gif?alpha=1&service=google_default");
            try {
                String cid = mail.embed(url, "Swiggy logo");
            }
            catch(Exception e){
                System.out.println("Not able to fetch logo for mail");
            }

            mail.setHtmlMsg(htmlmailbody);
            mail.setSubject(Subject);
            //mail.send();
            System.out.println("Mail successfully send");
        }
     catch (EmailException e) {
    e.printStackTrace();
} catch (MalformedURLException e) {
    e.printStackTrace();
}
    return mail;
    }



    private HtmlEmail MailerConfig(){
        HtmlEmail email = new HtmlEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(587);
        email.setAuthenticator(new DefaultAuthenticator("swiggytest@swiggy.in", "swiggytest@123"));
        email.setTLS(true);
        return email;
    }


}
