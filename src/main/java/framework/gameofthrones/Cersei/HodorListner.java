package framework.gameofthrones.Cersei;

import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.hudor.HodorHelper;
import framework.gameofthrones.hudor.HodorPage;
import org.testng.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.internal.MethodInvocationHelper;

import java.io.IOException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;

public class HodorListner implements IInvokedMethodListener {
    HodorPage hodorPage=new HodorPage();
    JsonHelper jsonHelper = new JsonHelper();
    HodorHelper hodorHelper = new HodorHelper();
    public static String testrunId;
    int count;
    boolean test;
    StringBuffer allException;
    String pod = "";
    Instant start,end;

    public HodorListner()
    {
        count = 0;
        allException = new StringBuffer();
        test = true;
    }
    @BeforeMethod(alwaysRun = true)
    public void beforeInvocation(IInvokedMethod invokedMethod, ITestResult testResult) {
        System.out.println("INSIDE THE BEFORE METHOD...");

        start = Instant.now();

        Method method = invokedMethod.getTestMethod().getConstructorOrMethod().getMethod();

        Test a = method.getAnnotation(Test.class);

        int hid=0;
        boolean isMerge = true;
        String[] data = {};
        try {
            isMerge = a.isMerge();
            System.out.println(isMerge);
            data = a.hodorId();
        }
        catch (Exception e)
        {
            System.out.println("before method running");
        }
        for(String idata:data)
        {
            hid++;
        }
        if (!isMerge)
            Assert.assertEquals(hid, MethodInvocationHelper.size,"The number of data provider is not matching with number of hodor ID");

    }

    @AfterMethod
    public void afterInvocation(IInvokedMethod invokedMethod, ITestResult result)
    {
        long automationTime;
        end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        System.out.println(timeElapsed.toMillis());
        automationTime = timeElapsed.toMillis();
        String serviceName="NA";
        String groupName = "NA";
        try {
            serviceName =  result.getTestContext().getCurrentXmlTest().getParameter("service-name");
            groupName = result.getTestContext().getCurrentXmlTest().getIncludedGroups().get(0);
        }
        catch (Exception e)
        {
            System.out.println("SERVICE NAME DOES NOT EXIST IN TESTNG XML FILE");
        }
        ITestContext ctx = result.getTestContext();
        System.out.println("INSIDE AFTER METHOD"+ ctx.getCurrentXmlTest().getParameter("core"));
        pod = ctx.getCurrentXmlTest().getParameter("core");
        Method method = invokedMethod.getTestMethod().getConstructorOrMethod().getMethod();

        Test a = method.getAnnotation(Test.class);
        String[] data = {};
        boolean isMerge=true;
        try {
            data = a.hodorId();
            isMerge = a.isMerge();
        }
        catch (Exception e)
        {
            System.out.println("no data..");
        }
        int size=0;

        if(MethodInvocationHelper.size==0)
            size=1;
        else
            size = MethodInvocationHelper.size;

        if(result.getStatus() == ITestResult.FAILURE) {
            allException.append(result.getThrowable());
        }
        if(isDpPresent(a) && data.length==1)
            updateDPResult("DP"+count,data[0],getStatus(result),pod,""+automationTime);
        else if(isDpPresent(a) && data.length>=1)
            updateDPResult("DP"+count,data[count],getStatus(result),pod,""+automationTime);
        else
            System.out.println("noting to update");


        if(isMerge) {
            if(!result.isSuccess() && test)
            {
                test = false;
            }

            if (count==size-1) {
                try {
                    if(data.length == 1 && isDpPresent(a))
                        updateResult(data[0], test, allException,pod,serviceName,groupName,automationTime);
                    else
                        updateResult(data[count], test, allException,pod,serviceName,groupName,automationTime);
                }
                catch (ArrayIndexOutOfBoundsException e)
                {
                    System.out.println("NO HODOR ID FOUND");
                    //throw new ArrayIndexOutOfBoundsException("PLEASE ADD HODOR-ID IN YOUR TEST METHOD");
                }
                test=true;
                allException = new StringBuffer();
                //return;
            }
            count++;
        }

        else
        {

            if (count<=size)
            {

                   String[] eachDpData = data[count].split("#");
                   for(String tcid:eachDpData)
                        updateResult(tcid, result.isSuccess(), allException, pod,serviceName,groupName,automationTime);
            }
            count++;
        }
        if(size == count) {
            count = 0;
            MethodInvocationHelper.size = 0;
            allException = new StringBuffer();
        }
    }

    public void updateResult(String tcId, boolean status,StringBuffer exception,String pod,String serviceName,String groupName,
                             long automationTime)
    {

        String sstatus="";
        if(status)
            sstatus="pass";
        else
            sstatus="fail";

        HodorPage updatedData = hodorPage.setDefaultData(tcId,testrunId,sstatus,pod,serviceName,groupName,""+automationTime);
        String json="";
        try {
            json = jsonHelper.getObjectToJSON(updatedData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            testrunId = hodorHelper.hodorUpdateResult(json);
        }
        catch (Exception e)
        {
            System.out.println("UNABLE TO GET RUNID");
        }
        if(status)
            System.out.println(tcId+ " result has updated as PASS");
        else
        {
            if(exception.length()!=0)
                System.out.println(tcId+ " result has updated as FAIL with reason:- "+exception.toString());
            else
                System.out.println(tcId+ " result has updated as FAIL");
        }
    }
    public void updateDPResult(String dpid, String tcId, String status,String pod,String automationTime)
    {


        String[] newTcId = tcId.split("#");
        for(String inTcId:newTcId) {
            HodorPage updatedData = hodorPage.setDefaultDPData(inTcId, dpid, testrunId, status, pod,automationTime);
            String json = "";
            try {
                json = jsonHelper.getObjectToJSON(updatedData);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                testrunId = hodorHelper.hodorUpdateResult(json);
            }
            catch (Exception e)
            {
                System.out.println("UNABLE TO GET RUNID");
            }
        }

//        if(status)
//            System.out.println(dpid+ " result has updated as PASS");
//        else
//        {
//            if(exception.length()!=0)
//                System.out.println(dpid+ " result has updated as FAIL with reason:- "+exception.toString());
//            else
//                System.out.println(dpid+ " result has updated as FAIL");
//        }
    }
    public String getStatus(ITestResult result)
    {
        String status= "";
        if(result.isSuccess())
            status="pass";
        else
            status="fail";
        return status;
    }

    public void verifyHodorLength(String[] data,int size)
    {

        if(data.length != size)
            Assert.fail("The length has been equal");

    }

    public boolean isDpPresent(Test a)
    {
        try
        {
           if(a.dataProvider().equalsIgnoreCase(""))
               return false;
           else
               return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static void main(String[] args) {
        Instant start = Instant.now();
//your code
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Instant end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        System.out.println("Time taken: "+ timeElapsed.toMillis() +" milliseconds");
    }
}
