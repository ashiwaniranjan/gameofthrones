package framework.gameofthrones.Baelish;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by ashish.bajpai on 14/12/17.
 */
@Rule(name = "Canonical Check", description = "Check the canonical Url" )
public class Canonical {
    private String targeturl;
    private SEOData seodata;

    /*public StatusCode(String URL){
        targeturl = URL;
    }*/

    public Canonical(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean canonical(@Fact("canonical") boolean canonical){
        return canonical;
    }

    @Action
    public void canonicalcheck(){
        Document doc = null;
        try {
            doc = Jsoup.connect(targeturl).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean returnval = false;
        String nodeattribute = doc.select("link[rel=canonical]").attr("href");
        int nodesize = doc.select("link[rel=canonical]").size();
        System.out.println("canonical Text = " + nodeattribute);
        if(nodeattribute.trim().length()>0 && nodesize > 0){
            returnval = true;
        }

        //return returnval;
    }



}
