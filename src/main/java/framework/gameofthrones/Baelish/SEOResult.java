package framework.gameofthrones.Baelish;

public class SEOResult {
	
	String[] H1URL=new String[10];
	String url;
	String statuscodetf;
	String indextf;
	String canonicaltf;
	String casesensitivetf;
	String urlwithoutwwwtf;
	String metatitletf;
	String metadescriptiontf;
	String h1counttf;
	String robottf;
	SEOEmail seoemail=new SEOEmail();
	int[] H1pass=new int[10];
	int[] H1fail=new int[10];
	int i=0;
	public static int total=0;
	public static int totalpass=0;
	public static int total2=0;
	public static int totalfail=0;
	
	//Getting if the check is true or false for a certain url
	public void getURL(String urlget){
		url=urlget;
	}
	public void getStatusCode(String scode){
		statuscodetf=scode;
	}
	public void getIndex(String index){
		indextf=index;
	}
	public void getCanonical(String cananocial){
		canonicaltf=cananocial;
	}
	public void getCaseSensitive(String casesensitive){
		casesensitivetf=casesensitive;
	}
	public void getUrlWithoutwww(String urlwithoutwww){
		urlwithoutwwwtf=urlwithoutwww;
	}
	public void getMetaTitle(String metatitle){
		metatitletf=metatitle;
	}
	public void getMetaDescription(String metadescription){
		metadescriptiontf=metadescription;
	}
	public void getH1Count(String h1count){
		h1counttf=h1count;
	}
	public void getRobot(String robot){
		robottf=robot;
	}
	
	//Calculating the total number of checks for a certain url
	public void checkTrue(){
		String[] all={statuscodetf,indextf,canonicaltf,casesensitivetf,urlwithoutwwwtf,metatitletf,
				metadescriptiontf,h1counttf,robottf};
		for(String x:all){
			
			if(x.equals("true") || x.equals("TRUE")){
				total++;
			}
			System.out.println("");
		}
		total2=total;
		//Sending total checks to email
		seoemail.total(url,total);
		total=0;
	}
	
	//Getting total pass for a url
	public void totalPass(){
		++totalpass;
		if(totalpass+totalfail==total2){
			seoemail.getpass(totalpass);
			total2=0;
			totalpass=0;
			totalfail=0;
		}
	}
	
	//Getting total fail and the failure urls along with what failed
	public void totalFail(String fail){
		++totalfail;
		if(totalpass+totalfail==total2){
			seoemail.getpass(totalpass);
			totalfail=0;
			totalpass=0;
			total2=0;
		}
		String failure=fail;
		seoemail.getfailurelist(failure);
	}
}
