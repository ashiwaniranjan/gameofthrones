package framework.gameofthrones.Baelish;

import framework.gameofthrones.Cersei.GoogleSheetHelper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashish.bajpai on 23/11/17.
 */
public class SEOHelper {
    public  List<SEOData> seochecks = new ArrayList<SEOData>();

    public SEOHelper() throws GeneralSecurityException, URISyntaxException, IOException {
    	
    	boolean excelsheet=true;
    	if(excelsheet){
    	dochandler dh=new dochandler();
    	
    	String[][] exceldata=dh.getData();
    	for(int i=0;i<exceldata.length;i++){
    		String category = exceldata[i][0];
            String subcategory = exceldata[i][1];
            String url = exceldata[i][2];

            String statuscode = exceldata[i][3];
            String mobileurl = exceldata[i][4];
            String index = exceldata[i][5];
            String noindex = exceldata[i][6];
            String canonical = exceldata[i][7];
            String androidurl = exceldata[i][8];
            String iosurl = exceldata[i][9];
            String urlcasesensitive = exceldata[i][10];
            String urlwithoutwww = exceldata[i][11];
            String metatitle = exceldata[i][12];
            String metadesc = exceldata[i][13];
            String robotstxt = exceldata[i][14];
            String ga = exceldata[i][15];
            String comscore = exceldata[i][16];
            String mobilefriendliness = exceldata[i][17];
            String headercheck = exceldata[i][18];
            String staticassets = exceldata[i][19];
            String check301 = exceldata[i][20];
            //System.out.println("subcategory = " + check301);
            String pagespeedscore = exceldata[i][21];
            String h1tagcount = exceldata[i][22];
            
            SEOData newrow = new SEOData(category, subcategory, url, statuscode, mobileurl, index, noindex,
                    canonical, androidurl, iosurl, urlcasesensitive, urlwithoutwww, metatitle, metadesc, robotstxt,
                    ga, comscore, mobilefriendliness, headercheck, staticassets, check301, pagespeedscore, h1tagcount);
            seochecks.add(newrow);
    	}
    	}
    	
    	else{
        String spreadsheetId = "1rFN4dg1osYzdUgt1h-swiFBUZOkIGeVgxlAYIWCDJSk";
        //holidayiq
        //String range = "urls!A2:W191";
        //swiggy
        String range = "swiggyurls!A2:W191";
        GoogleSheetHelper sheetAPI = new GoogleSheetHelper();
        try {
            List<List<Object>> values = sheetAPI.getSpreadSheetRecords(spreadsheetId, range);
            int count = 0;
            for (List<Object> row : values) {
                //System.out.println("row.get(2) = " + row.get(2));
                String category = row.get(0).toString().trim().toLowerCase();
                String subcategory = row.get(1).toString().trim().toLowerCase();
                String url = row.get(2).toString().trim().toLowerCase();

                String statuscode = row.get(3).toString().trim().toLowerCase();
                String mobileurl = row.get(4).toString().trim().toLowerCase();
                String index = row.get(5).toString().trim().toLowerCase();
                String noindex = row.get(6).toString().trim().toLowerCase();
                String canonical = row.get(7).toString().trim().toLowerCase();
                String androidurl = row.get(8).toString().trim().toLowerCase();
                String iosurl = row.get(9).toString().trim().toLowerCase();
                String urlcasesensitive = row.get(10).toString().trim().toLowerCase();
                String urlwithoutwww = row.get(11).toString().trim().toLowerCase();
                String metatitle = row.get(12).toString().trim().toLowerCase();
                String metadesc = row.get(13).toString().trim().toLowerCase();
                String robotstxt = row.get(14).toString().trim().toLowerCase();
                String ga = row.get(15).toString().trim().toLowerCase();
                String comscore = row.get(16).toString().trim().toLowerCase();
                String mobilefriendliness = row.get(17).toString().trim().toLowerCase();
                String headercheck = row.get(18).toString().trim().toLowerCase();
                String staticassets = row.get(19).toString().trim().toLowerCase();
                String check301 = row.get(20).toString().trim().toLowerCase();
                //System.out.println("subcategory = " + check301);
                String pagespeedscore = row.get(21).toString().trim().toLowerCase();
                String h1tagcount = row.get(22).toString().trim().toLowerCase();

                SEOData newrow = new SEOData(category, subcategory, url, statuscode, mobileurl, index, noindex,
                        canonical, androidurl, iosurl, urlcasesensitive, urlwithoutwww, metatitle, metadesc, robotstxt,
                        ga, comscore, mobilefriendliness, headercheck, staticassets, check301, pagespeedscore, h1tagcount);
                seochecks.add(newrow);
            }
        

        } catch (IOException e) {
            e.printStackTrace();
        }

    	}
    	}


}
