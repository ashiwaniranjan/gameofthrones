package framework.gameofthrones.Baelish;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class dochandler {
	static ArrayList<ArrayList<Object>> immutableList ;
	static String[][] listfinal;

	public String[][] getData() throws IOException {
		//System.out.println("path is "+System.getProperty("user.dir"));
		String path = System.getProperty("user.dir");
		File file=new File(path+"/src/test/resources/SEO_Test/SEOTests.xlsx");
		//System.out.print(file.getPath());
		FileInputStream ff=new FileInputStream(new File(file.toString()));


		//Get the workbook instance for XLSX file
		XSSFWorkbook workbook;

		

		workbook = new XSSFWorkbook(ff);

		XSSFSheet spreadsheet = workbook.getSheetAt(1);


		String totalrows=spreadsheet.getRow(0).getCell(2).getRawValue();
		
		listfinal=new String[Integer.parseInt(spreadsheet.getRow(0).getCell(2).getRawValue())][23];

		int a=0,b=0;
		for(int i=2;i<(Integer.parseInt(totalrows)+2);i++){

			b=0;
			for(int j=0;j<=22;j++){
				listfinal[a][b]=spreadsheet.getRow(i).getCell(j).toString();
				
				b++;
			}
			a++;	
		}



		return listfinal;
	}
}
